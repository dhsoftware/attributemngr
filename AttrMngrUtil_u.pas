unit AttrMngrUtil_u;
//Attribute Manager Utilities prepared for Datacad D4D
//Written by Joseph Sosnowski 2018.

//Base Template - V2/doMenu V2 Utilities template.pas
//unit used to hold constants, types and other data used by multiple units.
//instructions

//1.  Search and replace AttrMngr with unit name
//2.  Save the unit using the unit name
//3.  Add the unit to the project
//4.  Define the fields in the tAttrMngr_DR type.
interface

uses
  System.SysUtils, inifiles, //files & directory

  //Macro Units - Few if any units should be needed in this unit.
  //Use caution to avoid circular references

  //D4D required units
  uconstants,
  uvariables,
  urecords,
  uinterfaces,
  uinterfacesrecords;

const
  //DCALSTATES
  XMain = XDcalStateBegin;
  XAttrMngr = XMain + 1;

  //MACRO STATES {1}
  //Menu States
  //f1 through s9 for Datacad menu function keys are declared by D4D
  msMenuDone = 0;
  msMenuSkipRouter = 21;
  msMenuAgain = 22;

  //FORM STATES {2}
  fsFormStart = 30;
  fsFormActive = fsFormStart + 1; //Initialize the form
  fsFormFinish = fsFormStart + 2;  //execute final actions before exit
  fsFormExit = fsFormStart + 3;

  //Form Display modes
  dspDrawing  = 10;
  dspLayer = 11;
  dspSym = 12;
  dspEnt = 13;
  dspView = 14;
  dspDetail = 15;

  dmNone = 0;
  dmEntAtr = 1;
  dmAllAtr = 2;
  dmEntity = 3;
  dmAddAtr = 4;
  //ACTION STATES  - macro specific actions states
  asAction = 50;  //Sample  - establish values for your constants  that are unique for the macro.
  asGetPnts = asAction + 1; //Sample

  MAXPNTS = 36; //can be any positive value. Note that polygons are now allow 256 points.

  //.... others as required by the macro

type
tMain_LR = record
  state: integer;  //required to operate AttrMngr_doMenu
  menuMode,  //used by form onclosequery
  formLoaded, //Popup form has been created
  searchState, //used in select menu template

  firstesc, //handles multiple 'Esc' key calls
//  secondesc, //TODO is this needed?
  getFirstPnt,  //used when form manages macro
  gotPnts : boolean;  //used when form manages macro
  pntCount:   asint;  //current point being gathered
  Pnts2Get : integer; //number of points to get using getPoints loop in menus
  Pnts : array [1..maxPnts] of Point;  //maxPnts set to 36
  case byte of  //implement some common interfaces.
    //At least one is required to interact with the macro user
    0: (getp: getpointArg);  //Datacad get points or keyboard
    1: (gete: getescarg);  //keyboard input
    2: (getd: getdisarg);  //Datacad get distance menu
    3: (geta: getangarg);  //Datacad get angle menu
    4: (geti: getintarg);  //Datacad get an integer
    5: (getc: getclrarg);  //Datacad get standard color
    6: (gets: DgetstrArg);  //Datacad get a string
    7: (getr: getrealArg);  // Datacad get a real/float/double
    8: (getch: getchararg); //Datacad key is only result
    9: (getPoly : TPolyArg); //Datacad defining a polygon menu
  end;
pMain_LR = ^tMain_LR;  //required pointer

 tAttrMngr_DR = record
   //insert required fields for the macro
   doDrawing,
   doEntity,
   doLayer,
   doSymbol,
   doView,
   doDetail: boolean;
   adr : entaddr; // used to hold a selected entity
 end;
  pAttrMngr_DR = ^tAttrMngr_DR;

var
  file_ini : string;
  //include any variables to be used across the macro if at risk of circular unit referencing.
  AttrMngr_DR : tAttrMngr_DR;  {5}
implementation //---------------------------------------------------------------

procedure utilStartup;
var
  workPath : str255;
  iniFl: tIniFile;
begin
  //this sample code looks up Datacad's installation path, creates a new
  //subdirectory using the macro name and assigns the file name for a
  //AttrMngr.ini file to be used to save settings between macro uses.
  getpath (workPath, Pathinstall);  //Datacad installation directory
  workpath := workPath + 'AttrMngr\';
  if not DirectoryExists (string(workPath)) then
    MkDir (string(workPath));
  file_ini := string(workPath) + 'AttrMngr.ini';  //change the name for custom ini file. file_ini declared in conTEXTUtil_u
  //insert any additional startup code for this unit

	iniFl :=  TIniFile.Create(file_ini);  //create the class object
  try
    iniFl.writeBool ('ATTRMNGR RECORD', 'doDrawing', AttrMngr_DR.doDrawing);
    iniFl.writeBool ('ATTRMNGR RECORD', 'doEntity', AttrMngr_DR.doEntity);
    iniFl.writeBool ('ATTRMNGR RECORD', 'doLayer', AttrMngr_DR.doLayer);
    iniFl.writeBool ('ATTRMNGR RECORD', 'doSymbol', AttrMngr_DR.doSymbol);
    iniFl.writeBool ('ATTRMNGR RECORD', 'doView', AttrMngr_DR.doView);
    iniFl.writeBool ('ATTRMNGR RECORD', 'doDetail', AttrMngr_DR.doDetail);
  finally
  	IniFl.free;  //close the class object
  end;

end;

procedure utilShutdown;
var
  iniFl: tIniFile;
begin
	iniFl :=  TIniFile.Create(file_ini);  //create the class object
  try
    iniFl.writeBool ('ATTRMNGR RECORD', 'doDrawing', AttrMngr_DR.doDrawing);
    iniFl.writeBool ('ATTRMNGR RECORD', 'doEntity', AttrMngr_DR.doEntity);
    iniFl.writeBool ('ATTRMNGR RECORD', 'doLayer', AttrMngr_DR.doLayer);
    iniFl.writeBool ('ATTRMNGR RECORD', 'doSymbol', AttrMngr_DR.doSymbol);
    iniFl.writeBool ('ATTRMNGR RECORD', 'doView', AttrMngr_DR.doView);
    iniFl.writeBool ('ATTRMNGR RECORD', 'doDetail', AttrMngr_DR.doDetail);
  finally
  	IniFl.free;  //close the class object
  end;
end;

initialization
  utilStartup;
finalization
  utilShutdown;
end.
