unit AttrMngr_Menu_u;//AttrMngr_doMenu_u;
//prepared for Datacad D4D
//Written by Joseph Sosnowski 2018.


//Base Template V2/doMenu V2 wo Form.pas
//INSTRUCTIONS
//1.  Search and replace AttrMngr with the name of the unit and doMenu function.
//2.  Search for a and replace with a letter or string to identify the AttrMngr_doMEnu datarecord link variable
//3.  Verify that needed constants and types are included or visible from this unit
//4.  Declare a variable of type tAttrMngr_DR either here or other visible unit to hold the actual data for the macro.
//5.  Implement code for the AttrMngr_doMenu function.
//6.  Uncomment firstesc code if applicable (Search for firstEsc to find code lines).
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms,
  Vcl.StdCtrls, Vcl.dialogs, inifiles,

  //Macro Units
  AttrMngrUtil_u, //for Main_LR &other macro wide declarations
  atrBrowserForm_u,

  //D4D units
  uconstants,
  uvariables,
  urecords,
  uinterfaces,
  uinterfacesrecords;

//const
//Include a constant in the macro main library unit or the utility unit to identify
//the menu for use in the Main function. eg.  XAttrMngr = XMain + ??aValue??;

//Establish States in AttrMngrUtil_u for actions invoked in this menu using unique state
//numbers within the macro


procedure call_AttrMngr_doMenu (P_DR: pAttrMngr_DR; var iwant : wanttype);
function AttrMngr_doMenu(act : action; p_LR, p_DR : Pointer) : wantType;

//type  //if needed
{
//Establish a datarecord here or visible to the unit (eg. AttrMngrUtil_u) with this menu
  tAttrMngr_DR = record  //datarecord
    exampleStr : shortstring;
  end;
  pAttrMngr_DR = ^tAttrMngr_DR;
}
//var  //if needed
  //Use an existing data record in the menu or declare one specific for this menu.
  //Declare it in this unit or one that is identified in the uses section above
  //AttrMngr_DR : tAttrMngr_dr;

  //verify that file_ini is declared in the AttrMngrUtil_u.pas unit for use within startup and shutdown procedures
  //file_ini := 'C:\DataCAD 19\Support Files\AttrMngr\AttrMngr.ini';

implementation //----------------------------------------------------------

procedure call_AttrMngr_doMenu (P_DR: pAttrMngr_DR; var iwant : wanttype);
begin
  setargs (P_DR);
  iwant := XAttrMngr;
end;

function AttrMngr_doMenu(act : action; p_LR, p_DR : Pointer) : wantType;

var
   retval : asint;
   am_lr : pMain_LR;
   am_dr : pAttrMngr_DR;
   i : integer;
   ent : entity;
   mode : mode_type;
   procedure setAttrMngrlabels;
   begin
     wrtlvl('AttrMngr');  //Menu Title
     lblsinit;
//     lblset (1, ''); //
//     lblmsg (1, '');
//     lblset (2, ''); //
//     lblmsg (2, '');
//     lblset (3, ''); //
//     lblmsg (3, '');
     lblset (4, 'Use Form'); //
//     lblmsg (4, '');
//     lblset (5, ''); //
//     lblmsg (5, '');
//     lblset (6, ''); //
//     lblmsg (6, '');
//     lblset (7, ''); //
//     lblmsg (7, '');
//     lblset (8, ''); //
//     lblmsg (8, '');
//     lblset (9, ''); //
//     lblmsg (9, '');
//     lblset (10, ''); //
//     lblmsg (10, '');
//     lblset (11, ''); //
//     lblmsg (11, '');
//     lblset (12, ''); //
//     lblmsg (12, '');
//     lblset (13, ''); //
//     lblmsg (13, '');
//     lblset (14, ''); //
//     lblmsg (14, '');
//     lblset (15, ''); //
//     lblmsg (15, '');
//     lblset (16, ''); //
//     lblmsg (16, '');
//     lblset (17, ''); //
//     lblmsg (17, '');
//     lblset (18, ''); //
//     lblmsg (18, '');
//     lblset (19, ''); //
//     lblmsg (19, '');
     lblset (20, 'Exit');   //
     lblson;
   end;

   function CollectPnts : boolean;
   begin
     am_lr.pnts[am_lr.pntCount] := am_lr.getp.curs;
     if am_lr.pntCount < am_lr.Pnts2Get then begin
       am_lr.pntCount := am_lr.pntCount + 1;
       result := false;
     end
     else begin
       result := true; //All points gathered so take action. eg.  am_lr.state := ???which action???
       rubln^ := false; //optional
       am_lr.pntCount := 1; //reset for next iteration
     end;
   end;

begin
  am_lr := pMain_LR(p_LR);
  am_dr := addr(AttrMngr_DR); //first time. Secondary menus transfer to p_DR through call_menu functions.
//above or below
//   am_dr := pAttrMngr_DR(p_DR);  /for secondary menus where data is passed through p_DR.
                                              //START SECTION:
  case act of
                                                  //SIZING STEP
  alsize:
    begin
      SetLocalSize(sizeof(am_lr^));
    end;
                                                  //INITIALIZE MACRO STEP
  afirst:
    begin
      //define the mode for the menu/form
      am_lr.menuMode := false; //true= menu mode, false = form mode
      //FormMode means  form starts the menu operation
      //MenuMode means form is dormant at start
      //choose one of the two below
      //am_lr.state := msMenuAgain;  //for menu mode
      am_lr.state := fsFormStart;  //for form mode

      am_lr.Pnts2Get := 1; //set to correct amount
      am_lr.pntCount := 1;  //current point being gathered from user
      //TODO verify if these two lines from persist form version are needed here
      am_lr.gotpnts := false;
      am_lr.getFirstPnt  := true;
      for i := 1 to am_lr.Pnts2Get do
        setpoint (am_lr.Pnts [i], 0);
      wrterr('AttrMngr'); //Use if this is base menu used to display the macro name

    end;
                                                  //ROUTER STEP
  aagain:
    begin
      case am_lr.state of
      msMenuSkipRouter:
        begin
          am_lr.state := msMenuAgain;
        end;
      msMenuAgain:
        begin
          if am_lr.getp.Result = res_escape then begin
            //apply this if statement if using  am_lr.firstescape  in the menu
            //if (am_lr.getp.key) >= f1 and (am_lr.getp.key < s0) then
            //    am_lr.FirstEsc := false;

            case am_lr.getp.key of
//            f1 : am_lr.state := f1; //
//            f2 : am_lr.state := f2; //
//            f3 : am_lr.state := f3; //
//            f4 : am_lr.state := f4; //
//            f5 : am_lr.state := f5; //
//            f6 : am_lr.state := f6; //
//            f7 : am_lr.state := f7; //
//            f8 : am_lr.state := f8; //
//            f9 : am_lr.state := f9; //
//            f0 : am_lr.state := f0; //
//            s1 : am_lr.state := s1; //
//            s2 : am_lr.state := s2; //
//            s3 : am_lr.state := s3; //
//            s4 : am_lr.state := s4; //
//            s5 : am_lr.state := s5; //
//            s6 : am_lr.state := s6; //
//            s7 : am_lr.state := s7; //
//            s8 : am_lr.state := s8; //
//            s9 : am_lr.state := s9; //
            s0 :
            //apply this if statement if using  tm_lr.firstescape  in the menu
            //add an 'undoAction' state in EXECUTE STEPS if needed including tm_lrfirstesc := false;
              begin
                am_lr.state :=  msMenuDone; // Use this-S0 always works or below
{                if tm_lr.firstesc then begin  //time to leave
                  tm_lr.state :=  msMenuDone;
                end
                else begin  //first esc selected
                  tm_lr.firstesc := true;
                  //either keep current state or change to a new one as needed to act as required
                  tm_lr.state := asUndoAction;  //process the appropriate action if needed
                end;
}              end;
            end;
          end
          else if am_lr.getp.Result = res_normal then begin
            if collectPnts then begin
              mode_init (mode);
              mode_lyr (mode, lyr_on);
//              mode_enttype (mode, enttxt);
              if ent_near (ent, am_lr.pnts [1].x, am_lr.pnts[1].y, mode, true) then begin
                am_dr.adr := ent.addr;
              end;
              am_lr.state := fsFormActive; //process points with an appropriate action state
            end;
          end;
        end;

      //leave keys even if not used
      f1, f2, f3, f4, f5, f6, f7, f8, f9, f0, s1, s2, s3, s4,
      s5, s6, s7, s8, s9  : am_lr.state := msMenuAgain;

      //IMPORTANT - list every action state to avoid setting msMenuDone as default
      fsFormStart, fsFormActive, fsFormFinish, fsFormExit: ; //Do Nothing, form action states managed by popup

      else  //may include s0 option in else if firstesc not used
        am_lr.state := msMenuDone;
      end;
    end;
                                                  //SPECIAL EXIT STEP
  alast:
    begin

      am_lr.state := msMenuDone;
    end;
  end;
                                              //EXECUTE SECTION
  if act <> alsize then begin

    setAttrMngrlabels;
    case am_lr.state of
    msMenuAgain :                                 //DISPLAY STEP
      begin
//        tm_lr.firstesc := false;  //include if using firstesc
//        prompt the user for what to do
{        if am_lr.pntCount = 1 then begin
          wrtmsg('Select function key or enter first point');  //change prompt as needed
        end
        else begin
          wrtmsg('Enter next point');   //change prompt as needed
          rubln^ := true;
        end;
}
        wrtmsg('Select entity to view');            //INPUT STEP

        if am_lr.pntCount > 1 then
          rubln^ := false;//optional
        getpoint(am_lr.getp, retval);
      end;
                                                  //EXECUTE STEPS
    //Function key options
//Use  callnone (retval) if another retval is not assigned in the case
//    f1:  callnone (retval);
//    f2: callnone (retval);
//    f3: callnone (retval);
//    f4: callnone (retval);
//    f5: callnone (retval);
//    f6: callnone (retval);
//    f7: callnone (retval);
//    f8: callnone (retval);
//    f9: callnone (retval);
//    f0: callnone (retval);
//    s1: callnone (retval);
//    s2: callnone (retval);
//    s3: callnone (retval);
//    s4: callnone (retval);
//    s5: callnone (retval);
//    s6: callnone (retval);
//    s7: callnone (retval);
//    s8: callnone (retval);
//    s9: callnone (retval);


     //Form States
    fsFormStart:
      begin
        AttrMngrFormStart(am_lr,am_dr);
        callnone (retval);
      end;
    fsFormActive:      //31
      begin
        AttrMngrFormShow(am_lr,am_dr);
        callnone (retval);
      end;
    fsFormFinish:
      begin
        AttrMngrFormFinish(am_lr,am_dr);
        am_lr.state := fsFormExit;

        //add any cleanup activities before exiting this menu
        callnone (retval);
      end;
    fsFormExit:
      begin
        if am_lr.formLoaded then
          AttrMngrFormExit;

        //required
        retval := XDone;
      end;

    else
      retval := XDone;
    end;

  end;
  Result := retval;
                                                  //EXIT STEP
  if retval = Xdone then begin
    //add any cleanup activities before exiting this menu
  end;
end; //AttrMngr_doMenu

Procedure AttrMngrShutDown;
var
  iniFl: tIniFile;
begin
	iniFl :=  TIniFile.Create(file_ini);  //create the class object
  try
//     iniFl.writeInteger ('ATTRMNGR RECORD', 'anInteger', AttrMngr_DR.integerVar);
//     iniFl.writeFloat ('ATTRMNGR RECORD', 'aFloat', AttrMngr_DR.floatVar);
//     iniFl.writeString ('ATTRMNGR RECORD', 'aString', AttrMngr_DR.stringVar); //str8
//     iniFl.writeBool ('ATTRMNGR RECORD', 'aBool', AttrMngr_DR.boolVar);
  finally
  	IniFl.free;  //close the class object
  end;
end; //AttrMngrShutdown

Procedure AttrMngrStartUp;
var
  iniFl: tIniFile;
begin
  if fileexists(file_ini) then  begin  //change the name to match Datacad settings
    iniFl :=  TIniFile.Create(file_ini);  //create the class object
    try
//      AttrMngr_DR.integerVar := iniFl.readInteger ('ATTRMNGR RECORD', 'anInteger', 6 );
//      AttrMngr_DR.floatVar := iniFl.readFloat('ATTRMNGR RECORD', 'aFloat', 1.0);
//      AttrMngr_DR.StringVar := iniFl.readString ('ATTRMNGR RECORD', 'aString','error' );
//      AttrMngr_DR.BoolVar := iniFl.readBool ('ATTRMNGR RECORD', 'aBool',false );
    finally
	    IniFl.free;  //close the class object
    end;
  end
  else begin
    showmessage ('AttrMngr.ini not found');
//    init_AttrMngr;
  end;
end; //AttrMngrStartup

initialization
  AttrMngrStartUp;

finalization
  AttrMngrShutdown;
end.

