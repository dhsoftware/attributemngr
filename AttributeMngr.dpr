library AttributeMngr;
//Datacad D4D macro library unit
//Attribute Manager is an extension of Datacad example utility originally published
//by Mark Madura of Datacad.
//Extensions written by Joseph Sosnowski 2018.

//INSTRUCTIONS
//1. Add D4D units to the project
//2. Create AttrMngrUtil_u unit to hold macro constants, types and global variables and add it to the project
//3. Create MainMenu_u unit to hold the main menu for the macro and add it to the project
//4. Define a dcalstate in AttrMngrUtil_u for the new macro main menu
//5. Set Delphi IDE Project/Options/Delphi Compiler/Output Directory to your Datacad directory\macros
//6. Set Delphi IDE Project/Options/Debugger/Host Application to your Datacad directory\DCADwin.exe
uses
  System.SysUtils,
  System.Classes,
  AttrMngrUtil_u in 'AttrMngrUtil_u.pas',
  AttrMngr_Menu_u in 'AttrMngr_Menu_u.pas',
  atrBrowserForm_u in 'atrBrowserForm_u.pas' {atrForm},
  attr_u in 'Utilities\attr_u.pas',
  StringUtil_u in 'Utilities\StringUtil_u.pas',
  UConstants in '..\..\DCAL for Delphi Header Files\15\UConstants.pas',
  UDCLibrary in '..\..\DCAL for Delphi Header Files\15\UDCLibrary.pas',
  UInterfaces in '..\..\DCAL for Delphi Header Files\15\UInterfaces.pas',
  UInterfacesRecords in '..\..\DCAL for Delphi Header Files\15\UInterfacesRecords.pas',
  URecords in '..\..\DCAL for Delphi Header Files\15\URecords.pas',
  UVariables in '..\..\DCAL for Delphi Header Files\15\UVariables.pas';


{$R *.res}
{$E dmx}  //dmx is the extension for DCAL Dll's
function Main(dcalstate : asint; act : action; p_LR, p_DR : Pointer) : wantType; stdcall;
begin
   case dcalstate of
      XMain : Result := AttrMngr_doMenu(act, p_LR, p_DR);
      //add dcalstate cases with menu calls as required
      else
         Result := XDone;//Necessary/
   end;
end;

exports
  Main;{This is the entry function that will be called by DataCAD. All DCAL Dlls
  will have this function.}
end.
