unit StringUtil_u;
//String Utilities prepared for Datacad D4D
//Written by Joseph Sosnowski 2018.

//12/22/17 copied to context directory from Embarcadero/../Projects
//12/23/17 Revised content and moved to \D4D\Utilities
//3/14/18  Added/edited  DcadStrtoRadians & DcadRadtoStr
//3/14/18  Began  D4DUnitsToStr  to replace  DcadUnitsToDotStr,  DcadUnitsToStr
interface
uses
System.SysUtils, dialogs,
//conTEXTUtil_B1_u,
//dcalWrtUtil_u,
urecords,
uconstants;
const
  oneinch = 32.0;
  onemeter = 39.3700787*32;

  //in radians
  oneDegree = 0.0174533;
  oneMinute = 0.000290888;
  oneSecond = 0.000004848;

type
  tD4DSclType = (stArch, stEng, stDec, stInchFrc, stInchDec, stMetric, stMeters,
                  stCent, stMilli, stDin, stas1100);
  tD4DFltModeType = (mtFloat, mtDist, mtAngl);
  tD4DFltModeTypes = set of  tD4DFltModeType;

function addr2str (addr : entaddr): string;
function bool2str (b : boolean) : string;
function D4DUnitsToStr (num : afloat; SType : tD4DSclType; doDot : boolean): string;
function DcadClr2str (clr : integer): string;
function DcadRadtoStr (num : afloat; doDots : boolean): string;
function DcadStrtoRadians (workStr : string): afloat;
function DcadUnitsToDotStr (num : afloat): string;
function DcadUnitsToStr (num : afloat): string;
function DisplayString (text : string; len : integer) : string;
function EntTyp2Str (const entTyp : integer): string;
function FindPrevWord (str: string; var curpos : integer): boolean;
function findNextWord (str: string; var curpos : integer): boolean;
function getFraction (num: afloat) : string;
function Line2Str (const lineTyp : integer):string;
function pnt2str (pnt : point) : string;
function pointer2str (p : pointer): string;
function pushString (const origStr: string; const start : integer): string;
procedure splitStr (const inStr : string; cursor : integer; var str1, str2 : string);
function ssPtr2str (ss : ssPtrType): string;
Procedure stripSpace (var str : string);
function Substitute (Searchstr, ReplaceStr, InStr : string): string;
function TitleCase (const instr : string):string;

function txtFindFirst (const substr, str :string; var loc: integer): boolean;
function txtFindNext (const substr, str :string; var loc : integer): boolean;

var
  curTxtPos : integer; //for find in txtFindFirst & txtFindNext
implementation //---------------------------------------------------------------

function addr2str (addr : entaddr): string;
begin
  result := 'page: ' + inttostr(addr.page) + ' | ofs: '+ inttostr(addr.ofs);
end;

function bool2str (b : boolean) : string;
begin
  if b then
    result := 'true'
  else
    result := 'false';
end;

function DcadClr2str (clr : integer): string;

begin
  case clr of
    clrwhite: result := 'white';
    clrred: result := 'red';
    clrgrn: result := 'green';
    clrblue: result := 'blue';
    clrcyan: result := 'cyan';
    clrmgta: result := 'magenta';
    clrbrown: result := 'brown';
    clrltgray: result := 'lt. gray';
    clrdkgray: result := 'dk.gray';
    clrltred: result := 'lt. red';
    clrltgrn: result := 'lt. green';
    clrltblue: result := 'lt. blue';
    clrltcyan: result := 'lt. cyan';
    clrltmgta: result := 'lt. magenta';
    clryellow: result := 'yellow';
    else
      result := inttostr (clr);
  end;
end;

function DcadRadtoStr (num : afloat; doDots : boolean): string;
//(degrees * 0.0174533) + (minutes * 0.000290888) + (seconds * 0.000004848)
var
  dStr, mStr, sStr : string;
  tempFl, tempFl2, degrees, minutes : afloat;
  tempstr : string;
begin
  dstr := '';
  mStr := '';
  sStr := '';
  tempfl := 0;
  //calculate the units
  if num = 0 then
    if doDots then
      exit ('0.')
    else
      exit ('0'+#248);

  if num > 0 then begin
    degrees := trunc (num / onedegree);
    tempfl := degrees * onedegree;
    tempFl := num - tempFl;
    if doDots then
      dStr := floattostr(degrees) + '.'
    else
      dStr := floattostr(degrees) + #176 + ' ';
  end;

  if tempfl > 0 then begin
    Minutes := trunc (tempfl / oneMinute);
    tempFl := tempFl - (minutes * oneminute);
    if doDots then
      mStr := floattostr (minutes) + '.'
    else
      mStr := floattostr (minutes) + ''' ';
  end;

  if tempFl > 0 then begin
    if doDots then
      sStr := floattostr (round(tempFl/onesecond))
    else
      sStr := floattostr (round(tempFl/onesecond)) + '"';
  end;
  result :=  dstr + mstr + sstr;
end;

function DcadStrtoRadians (workStr : string): afloat;
//returns radians for the datacad input string degree.minutes.seconds
//assumes legal characters
var
  degrees, minutes, seconds : integer;
  len, p : integer;
  tempstr : string;

  procedure adjustAmounts;
  var
    amnt, remains : integer;
  begin
    if seconds >= 60 then begin
      amnt := seconds div 60;
      remains := seconds mod 60;
      seconds := seconds - (amnt * 60);
      minutes := minutes + amnt;
    end;
    if minutes >= 60 then begin
      amnt := minutes div 60;

      minutes := minutes - (amnt * 60);
      degrees := degrees + amnt;

    end;

  end;
begin
  degrees := 0;
  minutes := 0;
  seconds := 0;
  len := length (workstr);
  if len = 0 then begin
    exit (0);
  end;
  //find degrees
  p := Pos('.', workStr);
  if (p > 0) then begin
    tempstr := copy (workstr, 1, p - 1);
    degrees := strtoint (tempstr);
    Delete (workstr, 1, p);
  end
  else begin
    degrees := strtoint (workstr);
    workstr := '';
  end;

  //find minutes
  len := length (workstr);
  if len > 0 then begin
    p := Pos('.', workStr);

    if (p > 0) then begin
      tempstr := copy (workstr, 1, p - 1);
      minutes := strtoint (tempstr);
      Delete (workstr, 1, p);
    end
    else begin
      minutes := strtoint (workstr);
      workstr := '';
    end;
  end;

  //find seconds
  len := length (workstr);
  if len > 0 then begin
    seconds := strtoint (workstr);
  end;

  adjustAmounts;
  //calc the radians
  result :=  (degrees * 0.0174533) + (minutes * 0.000290888) + (seconds * 0.000004848);
end;


function D4DUnitsToStr (num : afloat; SType : tD4DSclType; doDot : boolean): string;
//1 meter = 39.3701"
//1" = 0.0254 meter

{scaleType
0 Architectural
1 Engineering
2 Decimal
3 Meters
4 Inches Fraction
5 Inches Decimal
6 Centimeters
7 Millimeters
8 Metric
9 DIN
10 AS 1100}
var

  ftStr, InchStr, fracStr : string;
  feet, inches, frac,tempFl : afloat;
begin
//  case PGSavevar.scaletype of     //cannot implement unitl in the macro

  case Stype of
  stArch:
    begin
      //calculate units
      if num > 0 then begin
        feet := trunc (num/onefoot);
        tempFl := num - (feet*onefoot);
        ftStr := floattostr(feet);
      end
      else begin
        exit ('Error');
      end;
      if tempfl > 0 then begin
        inches := trunc (tempfl/oneinch);
        InchStr := floattostr (inches);
      end
      else begin
        exit (ftStr+'.');
      end;
      frac := num - (feet*onefoot) - (inches*oneInch);

      //display Units
      if doDot then begin
        if frac > 0 then begin
          fracStr := floattostr(frac);
        end;
        result := '';
        if feet > 0 then begin
          if frac > 0 then begin
            result := ftstr + '.' + inchstr + '.' + fracstr;
          end
          else begin
            result := ftstr + '.' + inchstr;
          end;
        end
        else begin
          if frac > 0 then begin
            result := inchstr + '.' + fracstr;
          end
          else begin
            result := inchstr;
          end;
        end;
      end
      else begin
        if frac > 0 then begin
          fracStr := floattostr(frac);
        end;
        result := '';
        if feet > 0 then begin
          if frac > 0 then begin
            result := ftstr + '.' + inchstr + '.' + fracstr;
          end
          else begin
            result := ftstr + '.' + inchstr;
          end;
        end
        else begin
          if frac > 0 then begin
            result := inchstr + '.' + fracstr;
          end
          else begin
            result := inchstr;
          end;
        end;
      end;
    end;
  stEng:
    begin
      if doDot then begin

      end
      else begin

      end;
    end;
  stDec:
    begin
      if doDot then begin

      end
      else begin

      end;
    end;
  stInchFrc:
    begin
      if doDot then begin

      end
      else begin

      end;
    end;
  stInchDec:
    begin
      if doDot then begin

      end
      else begin

      end;
    end;
  stMetric:
    begin
      if doDot then begin

      end
      else begin

      end;
    end;
  stMeters:
    begin
      if doDot then begin

      end
      else begin

      end;
    end;
  stCent:
    begin
      if doDot then begin

      end
      else begin

      end;
    end;
  stMilli:
    begin
      if doDot then begin

      end
      else begin

      end;
    end;
  stDin:
    begin
      if doDot then begin

      end
      else begin

      end;
    end;
  stas1100:
    begin
    end;
//
  end;
end;

function DcadUnitsToDotStr (num : afloat): string;
var
  ftStr, InchStr, fracStr : string;
  feet, inches, frac,tempFl : afloat;
begin
//  case PGSavevar.scaletype of     //cannot implement unitl in the macro
//  0:  //Architectural   1'-10 5/8"
//    begin
      if num > 0 then begin
        feet := trunc (num/onefoot);
        tempFl := num - (feet*onefoot);
        ftStr := floattostr(feet);
      end
      else begin
        exit ('Error');
      end;

      if tempfl > 0 then begin
        inches := trunc (tempfl/oneinch);
        InchStr := floattostr (inches);
      end
      else begin
        exit (ftStr+'.');
      end;

      frac := num - (feet*onefoot) - (inches*oneInch);

      if frac > 0 then begin
        fracStr := floattostr(frac);
      end;
      result := '';
      if feet > 0 then begin
        if frac > 0 then begin
          result := ftstr + '.' + inchstr + '.' + fracstr;
        end
        else begin
          result := ftstr + '.' + inchstr;
        end;
      end
      else begin
        if frac > 0 then begin
          result := inchstr + '.' + fracstr;
        end
        else begin
          result := inchstr;
        end;
      end;


{    end;
  1:
    begin

    end;
  2:
    begin

    end;
  3:
    begin

    end;
  4:
    begin

    end;
  5:
    begin

    end;
  6:
    begin

    end;
  7:
    begin

    end;
  8:
    begin

    end;
  9:
    begin

    end;
  10:
    begin

    end;
  end;
  }
end;

function DcadUnitsToStr (num : afloat): string;
//convert a Datacad distance to a string
//12/13/17 Works for architectural
//TODO Additional imperial and all metric strings
//1 meter = 39.3701"
//1" = 0.0254 meter
{scaleType
0 Architectural
1 Engineering
2 Decimal
3 Meters
4 Inches Fraction
5 Inches Decimal
6 Centimeters
7 Millimeters
8 Metric
9 DIN
10 AS 1100}
var
  ftStr, InchStr, fracStr : string;
  feet, inches, frac,tempFl : afloat;
begin
//  case PGSavevar.scaletype of     //cannot implement unitl in the macro
//  0:  //Architectural   1'-10 5/8"
//    begin
      if num > 0 then begin
        feet := trunc (num/onefoot);
        tempFl := num - (feet*onefoot);
        ftStr := floattostr(feet);
      end
      else begin
        exit ('Error');
      end;

      if tempfl > 0 then begin
        inches := trunc (tempfl/oneinch);
        InchStr := floattostr (inches);
      end
      else begin
        exit (ftStr+'''');
      end;

      frac := num - (feet*onefoot) - (inches*oneInch);

      if frac > 0 then begin
        fracStr := getFraction (frac);
      end;
      result := '';
      if feet > 0 then begin
        if frac > 0 then begin
          result := ftstr + '''-' + inchstr + ' ' + fracstr + '"';
        end
        else begin
          result := ftstr + '''-' + inchstr+'"';
        end;
      end
      else begin
        if frac > 0 then begin
          result := inchstr + ' ' + fracstr + '"';
        end
        else begin
          result := inchstr + '"';
        end;
      end;


{    end;
  1:
    begin

    end;
  2:
    begin

    end;
  3:
    begin

    end;
  4:
    begin

    end;
  5:
    begin

    end;
  6:
    begin

    end;
  7:
    begin

    end;
  8:
    begin

    end;
  9:
    begin

    end;
  10:
    begin

    end;
  end;
  }
end;

function DisplayString (text : string; len : integer) : string;
begin
  if  length(text) > len then begin
    result := copy (text, 1, len - 3 ) + '...';
  end
  else begin
    result := text;
  end;
end;

function EntTyp2Str (const entTyp : integer): string;
begin
  case entTyp of
   entlin: result := 'Line';
   entcrc: result := 'Circle';
   entbez: result := 'Bezier Curve';
   entarc: result := 'Arc';
   entmrk: result := 'Mark';
   entell: result := 'Ellipse';
   enttxt: result := 'Text';
   entln3: result := '3d Line';
   entbsp: result := 'B-Spline';
   entdim: result := 'Dimension';
   entsym: result := 'Symbol';
   entxref: result := 'X-Ref';
   enthol: result := 'hol';
   entar3: result := '3d Arc';
   entply: result := 'Polygon';
   entblkattdef: result := 'entblkattdef';
   entcyl: result := 'Cylinder';
   entcon: result := 'Cone';
   entcnt: result := 'Contour Curve';
   enttor: result := 'Torus';
   entdom: result := 'Dome';
   entsrf: result := '3d Mesh Surface';
   entblk: result := 'Block';
   entslb: result := 'Slab';
   enttrn: result := 'Truncated Cone';
   entpln: result := 'Polyline';
   entrev: result := 'Surface of Revolution';

   entshl: result := 'entshl';
//   EntSuper: result := 'Super Entity';
  end;
end;

function FindPrevWord (str: string; var curpos : integer): boolean;
//Finds the end of the last word preceding the curpos
var
  done : boolean;
begin
  result := false;
  done := false;
  repeat
    if str [curpos] = ' ' then begin
      result := true;
      done := true;
    end
    else begin
      curpos :=   curpos - 1;
    end;
    if curpos = 0 then
      done := true;
  until done;
end;

function findNextWord (str: string; var curpos : integer): boolean;
//Finds the end of the last word after the curpos
var
  len : integer;
  done : boolean;
begin
  result := false;
  done := false;
  len := length (str);
  repeat
    inc(curpos);
    if str [curpos] = ' ' then begin
      result := true;
      done := true;
    end;
    if curpos = len then
      done := true;
  until done;
end;

function getFraction (num: afloat) : string;
var
  i : integer;
begin
  i := trunc (num);
  case i of
  0: result := '';
  1: result := '1/32';
  2: result := '1/16';
  3: result := '3/32';
  4: result := '1/8';
  5: result := '5/32';
  6: result := '3/16';
  7: result := '7/32';
  8: result := '1/4';
  9: result := '9/32';
  10: result := '5/16';
  11: result := '11/32';
  12: result := '3/8';
  13: result := '13/32';
  14: result := '7/16';
  15: result := '15/32';
  16: result := '1/2';
  17: result := '17/32';
  18: result := '9/16';
  19: result := '19/32';
  20: result := '5/8';
  21: result := '21/32';
  22: result := '11/16';
  23: result := '23/32';
  24: result := '3/4';
  25: result := '25/32';
  26: result := '13/16';
  27: result := '27/32';
  28: result := '7/8';
  29: result := '29/32';
  30: result := '15/16';
  31: result := '31/32';
  else
    result := 'Error';
  end;
end;

function Line2Str (const lineTyp : integer):string;
begin
  case lineTyp of
    ltype_solid: result := 'Solid'; //1
    ltype_dotted: result := 'Dotted';
    ltype_dashed: result := 'Dashed';
    ltype_dotdash: result := 'Dot Dash';
    else
      result := inttostr(lineTyp);
  end;
end;

function pnt2str (pnt : point) : string;
begin
  result := 'x: '+ floattostr(pnt.x)+ ' | y:'+ floattostr(pnt.y)+ ' | z:'+ floattostr(pnt.z)
end;

function pointer2str (p : pointer): string;
var
  Address: Integer;
begin
  Address := Integer(p);
  result := inttostr(Address);
end;

function pushString (const origStr: string; const start : integer): string;
//if string extends beyond margin then push origStr to result starting at start)
var
  len : integer;
begin
  len := length (origStr);
  result := copy (origStr, start, len);
end;


procedure splitStr (const inStr : string; cursor : integer; var str1, str2 : string);
//splits instr into two output strings  cleans proceding spaces from both
var
  done : boolean;
  len : integer;
begin
  done := false;
  len := length (instr);
  if cursor > 1 then
    str1 := copy (inStr, 1, cursor - 1)
  else
    str1 := '';

  str2 := copy (instr, cursor, len);
  repeat
    len := length (str1);
    if str1 [1] = '0' then
      str1 := copy (Str1, 2, len)
    else
      done := true;
  until done;
  repeat
    len := length (str2);
    if str2 [1] = '0' then
      str2 := copy (str2, 2, len)
    else
      done := true;
  until done;
end;

Procedure stripSpace (var str : string);
var
  len, i  : integer;
  workstr : string;
begin
  workstr := str;
  len := length (str);
  For i := 1 to len do begin
    if str [1] = ' ' then
      workStr := copy (str, i + 1, len)
    else
      break;
  end;
  str := workstr;
  len := length (str);
  For i := len downto 1 do begin
    if str [i] = ' ' then
      delete (str, i, 1)
    else break;
  end;
end;

function ssPtr2str (ss : ssPtrType): string;
begin
  result :=  addr2str (ss.ptr) + ' | ofs: '+ inttostr (ss.ofs);
end;

function Substitute (Searchstr, ReplaceStr, InStr : string): string;
var
  p, len : integer;
begin
  result := InStr;
  len := length (searchStr);
  p := pos (SearchStr, result);
  delete (result, p, len);
  insert (ReplaceStr, result, p);
end;

function TitleCase (const instr : string):string;
var
  i : integer;
  workStr :string;
begin
  result:='';
  workStr:=' '+instr;
  i:=1;
  repeat
    if workStr[i]<>' ' then
      result:=result+workStr[i];
    if workStr[i]=' ' then
    begin
      if (workStr[i+1]<>' ') then
      begin
        result:=result+workStr[i]+AnsiUpperCase(workStr[i+1]);
        Inc(i);
      end
      else
        result:=result+workStr[i];
    end;
    Inc(i);
  until i>Length(workStr)+1;
  Delete(result, 1, 1);
end;

function txtFindFirst (const substr, str :string; var loc: integer): boolean;
//find first position of substr in str
begin
  loc :=  pos (substr, str);
  curTxtPos := loc;  //???
  result := true;
  if loc = 0 then
    result := false;
end;

function txtFindNext (const substr, str :string; var loc : integer): boolean;
//find next position of substr. Value is count after the previous substring match.
var
  len, sublen, workpos : integer;
  workStr : string;
begin
  workPos := curTxtpos;
  len := length (str);
  sublen := length (substr);
  result := false;
  if len > 0 then begin
    workstr := copy(str, workPos + sublen, len);
    workpos :=  pos (substr, workStr);

    if workpos > 0 then begin  //found
      result := true;
      curTxtpos := curTxtpos + workpos;
      loc := curTxtPos;
    end;
  end;
end;

end.
