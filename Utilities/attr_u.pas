unit attr_u;
//Attribute Utilities prepared for Datacad D4D
//Written by Joseph Sosnowski 2018.

{
//conTEXT DRAWING ATTRIBUTE STRUCTURES
//conTEXT attributes are held in strings and parsed using tstringlist using '|' as separator

  //DRAWING ATTRIBUTES
  Drawing ID - afloat|parent|object type|TableCount -  integer|lastedit

  T1 - ID|Parent|name|lastedit|filename

  ScheduleCount - integer
  S1 - ID|Parent|name|layerID|lastedit|filename
  S1Stl - layout settings

  //ENTITY ATTRIBUTES
  ID - afloat|object type|lastedit
  Parent - afloat
  format - string (varies by type)
    style (see ctFmt2Str)

}

{from urecords.pas
attrib = packed record
      atrtype: aSInt; //Values not defined in DCAl Manual
      addr: atraddr;
      addrSpc: adsmem;
      Next: atraddr;
      prev: atraddr;
      Name: atrname;
      Visible: boolean;
      SPBvisible: Boolean; // Solid pattern bitmap fill boundary visible
      VarDataSize: Byte; // max space for str255 is 255
      positn: point;
      txtcolor: aSInt;
      txtsize: aFloat;
      txtang: aFloat;
      txtslant: aFloat;
      txtaspect: aFloat;
      CASE aSInt OF
         atr_str: (str: str80);
         atr_int: (int: aSInt);
         atr_rl: (rl: aFloat);
         atr_dis: (dis: aFloat);
         atr_ang: (ang: aFloat);
         atr_pnt: (pnt: point);
         atr_addr: (lgladdr: lgl_addr);
         atr_db: (db: db_article);
         atr_str255: (shstr: str255);
   END;
}
{from uconstants.pas
   atr_name_len = 12;
   atr_int = 0;
   atr_rl = 1;
   atr_str = 2;
   atr_dis = 3;
   atr_ang = 4;
   atr_pnt = 5;
   atr_addr = 6; // Should not be used
   atr_db = 7;
   atr_str255 = 8;
   atr_AreaPerim = 9;
}

interface
uses
 system.classes,  //tstringlist
 system.SysUtils,
 vcl.dialogs,  //showmessage

 uconstants,
 urecords,
 uinterfaces;

//TODO This may not be necessary
// conTEXTUtil_B1_u;

const
  stEnt = 1;
  stLyr = 2;
  stDwg = 3;
  stSym = 4;

type
tAttrSource = record
   case sType : asint of
     stent: (ent: entity);
     stLyr: (lyr:layer);
     stDwg:(); // (isDwg: boolean);
     stSym: (sym: symbol);
   end;

tCTxtFmt = record
  Parent,
  ID : afloat; //was IDStamp string
  first, //first entity in series
  //text formats
  underline,
  overline,
  box,
  para : boolean;
  Otype,  //Ashlar Dwg object type  - see conTEXTUtil_B1_u
  wrap,
  tabWidth,
  tabPos,
  lineNo,
  just : integer;
  justChar : str8;
  taboffset,
  linefeed : afloat;
  TblTitle, Filename : string;
end;

tAttrDef = record
  source : tAttrsource; //embedded case record
  name : string;
  visible : boolean;

  //standard DCAL attributes
  case kind : asint of
   atr_int : (int: integer);// = 0;
   atr_rl : (rl : afloat); //= 1;
   atr_str : (str : str80); //= 2;
   atr_dis : (dis : afloat); //= 3;
   atr_ang : (ang : afloat); //= 4;
   atr_pnt : (pnt : point);//= 5;
   atr_addr : (adr : entaddr);//= 6; // Should not be used
//   atr_db //= 7;
   atr_str255 : (ShStr : str255);//= 8;
//   atr_AreaPerim //= 9;
   end;

function ctFmt2Str (cTxtFmt: tCTxtFmt) : string;
function  ctStr2Fmt (str : string; CTxtFmt : tCTxtFmt ): boolean;
procedure WrtAttr2 (var AttrDef : tAttrDef; overwrite : boolean);

implementation


function ctFmt2Str (cTxtFmt: tCTxtFmt) : string;
// lineNo, linefeed, wordWrap, just, justchar, New Paragraph, tabwidth, tabpos,
//tabspace, underline, overline, box
//just = justification alighment mode
//wordwrap 0 = off
//tabpos 0 = none
begin
  result := inttoStr (cTxtFmt.lineNo)+'|';//style is placed in the 'type' attr
  result := result +  floattoStr (cTxtFmt.linefeed)+'|';
  result := result + inttostr (cTxtFmt.wrap)+'|';
  result := result + inttostr(cTxtFmt.just)+'|';
  result := result + string(cTxtFmt.justChar +'|');
  if cTxtFmt.para then
    result := result + '1|'
  else
    result := result + '0|';
  result := result + inttostr(cTxtFmt.tabwidth)+'|';
  result := result + inttostr(cTxtFmt.tabpos)+'|';
  result := result + floattostr(cTxtFmt.taboffset);
  if cTxtFmt.underline then
    result := result + '1|'
  else
    result := result + '0|';
  if cTxtFmt.overline then
    result := result + '1|'
  else
    result := result + '0|';
  if cTxtFmt.box then
    result := result + '1|'
  else
    result := result + '0|';

end;

function  ctStr2Fmt (str : string; CTxtFmt : tCTxtFmt ): boolean;
var
  List:  TStringList;
begin
  List := TStringList.Create;
  try
    List.Clear;
    List.Delimiter       := '|';
    List.StrictDelimiter := True; // Requires D2006 or newer.
    List.DelimitedText   := Str;

    if list.count <> 12 then  begin
      showmessage ('Error reading Format attribute');
      result := false;
      exit;
    end;

    cTxtFmt.lineNo := strtoint (List [0]);
    cTxtFmt.linefeed := StrToFloat(List [1]);
    cTxtFmt.just :=  StrToint (List [2]);
    cTxtFmt.justchar := str8(List [3]);
    cTxtFmt.wrap := StrToInt(List [4]);
    if list [5] = '1' then
      cTxtFmt.para := true
    else
      cTxtFmt.para := false;
    cTxtFmt.tabWidth := strtoint (list [6]);
    cTxtFmt.tabPos := strtoint (List [7]);
    cTxtFmt.tabOffset := strtofloat (List [8]);
    if list [9] = '1' then
      cTxtFmt.underline := true
    else
      cTxtFmt.underline := false;
    if list [10] = '1' then
      cTxtFmt.overline := true
    else
      cTxtFmt.overline := false;
    if list [11] = '1' then
      cTxtFmt.box := true
    else
      cTxtFmt.box := false;
  finally
    List.Free;
  end;
  result := true;
end;

procedure WrtAttr2 (var AttrDef : tAttrDef; overwrite : boolean);

var
  WorkAtr : attrib;
  adr : lgl_addr;
begin
  atr_init  (workAtr, AttrDef.kind);
  adr := AttrDef.source.ent.addr;
  case AttrDef.Source.Stype of
  stEnt:
    begin
      if (atr_entfind (AttrDef.source.ent, atrname(AttrDef.name), WorkAtr))then begin //Attr exists
        if AttrDef.name = 'ID' then // never overwrite the ID.
          exit;
        if not overwrite then
           exit
        else begin //overwrite = true
          case AttrDef.kind of
          atr_str:
            begin
              WorkAtr.str := AttrDef.str;
              atr_update (workatr);
            end;
          atr_int:
            begin
              WorkAtr.int := AttrDef.int;
              atr_update (WorkAtr);
            end;//int: integer;
          atr_rl:
            begin
              WorkAtr.rl := AttrDef.rl;
              atr_update (WorkAtr);
            end;//rl: afloat;
          atr_dis:
            begin
              WorkAtr.dis := AttrDef.dis;
              atr_update (WorkAtr);
            end;//dis: afloat;
          atr_ang:
            begin
              WorkAtr.ang := AttrDef.ang;
              atr_update (WorkAtr);
            end;//ang: afloat;
          atr_pnt:
            begin
              WorkAtr.pnt := AttrDef.pnt;
              atr_update (WorkAtr);
            end;//pnt : point;
          atr_str255:
            begin
              WorkAtr.ShStr := AttrDef.ShStr;
              atr_update (WorkAtr);
            end;//Shstr : str255;
          end;
        end; //overwrite false
      end
      else begin //new attribute

        case AttrDef.kind of
        atr_str:
          begin
            WorkAtr.name := atrname(AttrDef.name);
            WorkAtr.atrtype := atr_str;
            WorkAtr.str := AttrDef.str;
            WorkAtr.visible := false;
            atr_add2ent (AttrDef.source.ent, WorkAtr);
          end;
        atr_int:
          begin
            WorkAtr.name := atrname(AttrDef.name);
            WorkAtr.atrtype := atr_int;
            WorkAtr.int := AttrDef.int;
            WorkAtr.visible := false;
            atr_add2ent (AttrDef.source.ent, WorkAtr);
          end;//int: integer;
        atr_rl:
          begin
            WorkAtr.name := atrname(AttrDef.name);
            WorkAtr.atrtype := atr_rl;
            WorkAtr.rl := AttrDef.rl;
            WorkAtr.visible := false;
            atr_add2ent (AttrDef.source.ent, WorkAtr);
          end;//rl: afloat;
        atr_dis:
          begin
            WorkAtr.name := atrname(AttrDef.name);
            WorkAtr.atrtype := atr_dis;
            WorkAtr.dis := AttrDef.dis;
            WorkAtr.visible := false;
            atr_add2ent (AttrDef.source.ent, WorkAtr);
          end;//dis: afloat;
        atr_ang:
          begin
            WorkAtr.name := atrname(AttrDef.name);
            WorkAtr.atrtype := atr_ang;
            WorkAtr.ang := AttrDef.ang;
            WorkAtr.visible := false;
            atr_add2ent (AttrDef.source.ent, WorkAtr);
          end;//ang: afloat;
        atr_pnt:
          begin
            WorkAtr.name := atrname(AttrDef.name);
            WorkAtr.atrtype := atr_pnt;
            WorkAtr.pnt := AttrDef.pnt;
            WorkAtr.visible := false;
            atr_add2ent (AttrDef.source.ent, WorkAtr);
          end;//pnt : point;
        atr_str255:
          begin
            WorkAtr.name := atrname(AttrDef.name);
            WorkAtr.atrtype := atr_str255;
            WorkAtr.ShStr := AttrDef.ShStr;
            WorkAtr.visible := false;
            atr_add2ent (AttrDef.source.ent, WorkAtr);
          end;//Shstr : str255;
        end;
      end;
      adr :=  workAtr.addr;
    end;
  stLyr:
    begin
      if (atr_lyrfind (AttrDef.source.lyr, atrname(AttrDef.name), WorkAtr))then begin //Attr exists
        if not overwrite then
           exit
        else begin //overwrite = true
          case AttrDef.kind of
          atr_str:
            begin
              WorkAtr.str := AttrDef.str;
              atr_update (workatr);
            end;
          atr_int:
            begin
              WorkAtr.int := AttrDef.int;
              atr_update (WorkAtr);
            end;//int: integer;
          atr_rl:
            begin
              WorkAtr.rl := AttrDef.rl;
              atr_update (WorkAtr);
            end;//rl: afloat;
          atr_dis:
            begin
              WorkAtr.dis := AttrDef.dis;
              atr_update (WorkAtr);
            end;//dis: afloat;
          atr_ang:
            begin
              WorkAtr.ang := AttrDef.ang;
              atr_update (WorkAtr);
            end;//ang: afloat;
          atr_pnt:
            begin
              WorkAtr.pnt := AttrDef.pnt;
              atr_update (WorkAtr);
            end;//pnt : point;
          atr_str255:
            begin
              WorkAtr.ShStr := AttrDef.ShStr;
              atr_update (WorkAtr);
            end;//Shstr : str255;
          end;
        end; //overwrite false
      end
      else begin //new attribute
        case AttrDef.kind of
        atr_str:
          begin
            WorkAtr.name := atrname(AttrDef.name);
            WorkAtr.atrtype := atr_str;
            WorkAtr.str := AttrDef.str;
            WorkAtr.visible := false;
            atr_add2lyr (AttrDef.source.lyr, WorkAtr);
          end;
        atr_int:
          begin
            WorkAtr.name := atrname(AttrDef.name);
            WorkAtr.atrtype := atr_int;
            WorkAtr.int := AttrDef.int;
            WorkAtr.visible := false;
            atr_add2lyr (AttrDef.source.lyr, WorkAtr);
          end;//int: integer;
        atr_rl:
          begin
            WorkAtr.name := atrname(AttrDef.name);
            WorkAtr.atrtype := atr_rl;
            WorkAtr.rl := AttrDef.rl;
            WorkAtr.visible := false;
            atr_add2lyr (AttrDef.source.lyr, WorkAtr);
          end;//rl: afloat;
        atr_dis:
          begin
            WorkAtr.name := atrname(AttrDef.name);
            WorkAtr.atrtype := atr_dis;
            WorkAtr.dis := AttrDef.dis;
            WorkAtr.visible := false;
            atr_add2lyr (AttrDef.source.lyr, WorkAtr);
          end;//dis: afloat;
        atr_ang:
          begin
            WorkAtr.name := atrname(AttrDef.name);
            WorkAtr.atrtype := atr_ang;
            WorkAtr.ang := AttrDef.ang;
            WorkAtr.visible := false;
            atr_add2lyr (AttrDef.source.lyr, WorkAtr);
          end;//ang: afloat;
        atr_pnt:
          begin
            WorkAtr.name := atrname(AttrDef.name);
            WorkAtr.atrtype := atr_pnt;
            WorkAtr.pnt := AttrDef.pnt;
            WorkAtr.visible := false;
            atr_add2lyr (AttrDef.source.lyr, WorkAtr);
          end;//pnt : point;
        atr_str255:
          begin
            WorkAtr.name := atrname(AttrDef.name);
            WorkAtr.atrtype := atr_str255;
            WorkAtr.ShStr := AttrDef.ShStr;
            WorkAtr.visible := false;
            atr_add2lyr (AttrDef.source.lyr, WorkAtr);
          end;//Shstr : str255;
        end;
      end;
    end;
  stDwg:
    begin
      if (atr_sysfind (atrname(AttrDef.name), WorkAtr))then begin //Attr exists
        if not overwrite then
           exit
        else begin //overwrite = true
          case AttrDef.kind of
          atr_str:
            begin
              WorkAtr.str := AttrDef.str;
              atr_update (workatr);
            end;
          atr_int:
            begin
              WorkAtr.int := AttrDef.int;
              atr_update (WorkAtr);
            end;//int: integer;
          atr_rl:
            begin
              WorkAtr.rl := AttrDef.rl;
              atr_update (WorkAtr);
            end;//rl: afloat;
          atr_dis:
            begin
              WorkAtr.dis := AttrDef.dis;
              atr_update (WorkAtr);
            end;//dis: afloat;
          atr_ang:
            begin
              WorkAtr.ang := AttrDef.ang;
              atr_update (WorkAtr);
            end;//ang: afloat;
          atr_pnt:
            begin
              WorkAtr.pnt := AttrDef.pnt;
              atr_update (WorkAtr);
            end;//pnt : point;
          atr_str255:
            begin
              WorkAtr.ShStr := AttrDef.ShStr;
              atr_update (WorkAtr);
            end;//Shstr : str255;
          end;
        end; //overwrite false
      end
      else begin //new attribute
        case AttrDef.kind of
        atr_str:
          begin
            WorkAtr.name := atrname(AttrDef.name);
            WorkAtr.atrtype := atr_str;
            WorkAtr.str := AttrDef.str;
            WorkAtr.visible := false;
            atr_add2sys (WorkAtr);
          end;
        atr_int:
          begin
            WorkAtr.name := atrname(AttrDef.name);
            WorkAtr.atrtype := atr_int;
            WorkAtr.int := AttrDef.int;
            WorkAtr.visible := false;
            atr_add2sys (WorkAtr);
          end;//int: integer;
        atr_rl:
          begin
            WorkAtr.name := atrname(AttrDef.name);
            WorkAtr.atrtype := atr_rl;
            WorkAtr.rl := AttrDef.rl;
            WorkAtr.visible := false;
            atr_add2sys (WorkAtr);
          end;//rl: afloat;
        atr_dis:
          begin
            WorkAtr.name := atrname(AttrDef.name);
            WorkAtr.atrtype := atr_dis;
            WorkAtr.dis := AttrDef.dis;
            WorkAtr.visible := false;
            atr_add2sys (WorkAtr);
          end;//dis: afloat;
        atr_ang:
          begin
            WorkAtr.name := atrname(AttrDef.name);
            WorkAtr.atrtype := atr_ang;
            WorkAtr.ang := AttrDef.ang;
            WorkAtr.visible := false;
            atr_add2sys (WorkAtr);
          end;//ang: afloat;
        atr_pnt:
          begin
            WorkAtr.name := atrname(AttrDef.name);
            WorkAtr.atrtype := atr_pnt;
            WorkAtr.pnt := AttrDef.pnt;
            WorkAtr.visible := false;
            atr_add2sys (WorkAtr);
          end;//pnt : point;
        atr_str255:
          begin
            WorkAtr.name := atrname(AttrDef.name);
            WorkAtr.atrtype := atr_str255;
            WorkAtr.ShStr := AttrDef.ShStr;
            WorkAtr.visible := false;
            atr_add2sys (WorkAtr);
          end;//Shstr : str255;
        end;
      end;
    end;
  stSym:
    begin
      if (atr_symfind (AttrDef.source.sym, atrname(AttrDef.name), WorkAtr))then begin //Attr exists
        if not overwrite then
           exit
        else begin //overwrite = true
          case AttrDef.kind of
          atr_str:
            begin
              WorkAtr.str := AttrDef.str;
              atr_update (workatr);
            end;
          atr_int:
            begin
              WorkAtr.int := AttrDef.int;
              atr_update (WorkAtr);
            end;//int: integer;
          atr_rl:
            begin
              WorkAtr.rl := AttrDef.rl;
              atr_update (WorkAtr);
            end;//rl: afloat;
          atr_dis:
            begin
              WorkAtr.dis := AttrDef.dis;
              atr_update (WorkAtr);
            end;//dis: afloat;
          atr_ang:
            begin
              WorkAtr.ang := AttrDef.ang;
              atr_update (WorkAtr);
            end;//ang: afloat;
          atr_pnt:
            begin
              WorkAtr.pnt := AttrDef.pnt;
              atr_update (WorkAtr);
            end;//pnt : point;
          atr_str255:
            begin
              WorkAtr.ShStr := AttrDef.ShStr;
              atr_update (WorkAtr);
            end;//Shstr : str255;
          end;
        end; //overwrite false
      end
      else begin //new attribute
        case AttrDef.kind of
        atr_str:
          begin
            WorkAtr.name := atrname(AttrDef.name);
            WorkAtr.atrtype := atr_str;
            WorkAtr.str := AttrDef.str;
            WorkAtr.visible := false;
            atr_add2sym (AttrDef.source.sym, WorkAtr);
          end;
        atr_int:
          begin
            WorkAtr.name := atrname(AttrDef.name);
            WorkAtr.atrtype := atr_int;
            WorkAtr.int := AttrDef.int;
            WorkAtr.visible := false;
            atr_add2sym (AttrDef.source.sym, WorkAtr);
          end;//int: integer;
        atr_rl:
          begin
            WorkAtr.name := atrname(AttrDef.name);
            WorkAtr.atrtype := atr_rl;
            WorkAtr.rl := AttrDef.rl;
            WorkAtr.visible := false;
            atr_add2sym (AttrDef.source.sym, WorkAtr);
          end;//rl: afloat;
        atr_dis:
          begin
            WorkAtr.name := atrname(AttrDef.name);
            WorkAtr.atrtype := atr_dis;
            WorkAtr.dis := AttrDef.dis;
            WorkAtr.visible := false;
            atr_add2sym (AttrDef.source.sym, WorkAtr);
          end;//dis: afloat;
        atr_ang:
          begin
            WorkAtr.name := atrname(AttrDef.name);
            WorkAtr.atrtype := atr_ang;
            WorkAtr.ang := AttrDef.ang;
            WorkAtr.visible := false;
            atr_add2sym (AttrDef.source.sym, WorkAtr);
          end;//ang: afloat;
        atr_pnt:
          begin
            WorkAtr.name := atrname(AttrDef.name);
            WorkAtr.atrtype := atr_pnt;
            WorkAtr.pnt := AttrDef.pnt;
            WorkAtr.visible := false;
            atr_add2sym (AttrDef.source.sym, WorkAtr);
          end;//pnt : point;
        atr_str255:
          begin
            WorkAtr.name := atrname(AttrDef.name);
            WorkAtr.atrtype := atr_str255;
            WorkAtr.ShStr := AttrDef.ShStr;
            WorkAtr.visible := false;
            atr_add2sym (AttrDef.source.sym, WorkAtr);
          end;//Shstr : str255;
        end;
      end;
    end;
  end;
end;   //WrtAttr2

end.
