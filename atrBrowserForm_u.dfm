object AttrMngrForm: TAttrMngrForm
  Left = 0
  Top = 0
  Caption = 'Attribute Browser'
  ClientHeight = 536
  ClientWidth = 526
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 526
    Height = 536
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 424
    object TabSheet1: TTabSheet
      Caption = 'Entities'
      ExplicitWidth = 416
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 518
        Height = 81
        Align = alTop
        Caption = 'Panel2'
        ShowCaption = False
        TabOrder = 0
        ExplicitWidth = 416
        DesignSize = (
          518
          81)
        object EntityButton: TButton
          Left = 256
          Top = 17
          Width = 75
          Height = 25
          Caption = 'Refresh'
          TabOrder = 0
          OnClick = EntityButtonClick
        end
        object RadioGroup1: TRadioGroup
          Left = 8
          Top = 4
          Width = 233
          Height = 71
          Caption = 'Selection Type'
          Columns = 3
          ItemIndex = 5
          Items.Strings = (
            'Drawing'
            'Layer'
            'View'
            'Symbol'
            'Detail'
            'Entity')
          TabOrder = 1
          OnClick = RadioGroup1Click
        end
        object ActiveLyrCheckBox: TCheckBox
          Left = 256
          Top = 48
          Width = 114
          Height = 17
          Caption = 'Active Layer Only'
          TabOrder = 2
        end
        object Button1: TButton
          Left = 467
          Top = 17
          Width = 43
          Height = 25
          Anchors = [akTop, akRight]
          Cancel = True
          Caption = 'Exit'
          ModalResult = 2
          TabOrder = 3
          ExplicitLeft = 448
        end
      end
      object EntValueListEditor: TValueListEditor
        Left = 0
        Top = 81
        Width = 518
        Height = 427
        Align = alClient
        TabOrder = 1
        ExplicitWidth = 502
        ColWidths = (
          150
          362)
        RowHeights = (
          18
          18)
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Attributes'
      ImageIndex = 1
      ExplicitWidth = 469
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 518
        Height = 97
        Align = alTop
        TabOrder = 0
        ExplicitWidth = 469
        DesignSize = (
          518
          97)
        object Label1: TLabel
          Left = 32
          Top = 70
          Width = 31
          Height = 13
          Caption = 'Label1'
        end
        object EntCheckBox: TCheckBox
          Left = 121
          Top = 11
          Width = 97
          Height = 17
          Caption = 'Show Entities'
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
        object RefreshAtrButton: TButton
          Left = 435
          Top = 7
          Width = 75
          Height = 25
          Anchors = [akTop, akRight]
          Caption = 'Refresh'
          TabOrder = 1
          OnClick = RefreshAtrButtonClick
          ExplicitLeft = 416
        end
        object EntityAtrButton: TButton
          Left = 435
          Top = 38
          Width = 75
          Height = 25
          Anchors = [akTop, akRight]
          Caption = 'Entity'
          TabOrder = 2
          OnClick = EntityAtrButtonClick
          ExplicitLeft = 416
        end
        object DtlCheckBox: TCheckBox
          Left = 224
          Top = 34
          Width = 97
          Height = 17
          Caption = 'Show Detail'
          Checked = True
          State = cbChecked
          TabOrder = 3
        end
        object DwgCheckBox: TCheckBox
          Left = 16
          Top = 11
          Width = 82
          Height = 17
          Caption = 'Show Drawing'
          Checked = True
          State = cbChecked
          TabOrder = 4
        end
        object LyrCheckBox: TCheckBox
          Left = 121
          Top = 34
          Width = 82
          Height = 17
          Caption = 'Show Layer'
          Checked = True
          State = cbChecked
          TabOrder = 5
        end
        object SymCheckBox: TCheckBox
          Left = 16
          Top = 34
          Width = 82
          Height = 17
          Caption = 'Show Symbol'
          Checked = True
          State = cbChecked
          TabOrder = 6
        end
        object VueCheckBox: TCheckBox
          Left = 224
          Top = 13
          Width = 97
          Height = 17
          Caption = 'Show View'
          Checked = True
          State = cbChecked
          TabOrder = 7
        end
        object Button2: TButton
          Left = 467
          Top = 69
          Width = 43
          Height = 22
          Anchors = [akTop, akRight]
          Cancel = True
          Caption = 'Exit'
          ModalResult = 2
          TabOrder = 8
          ExplicitLeft = 448
        end
      end
      object AtrValueListEditor: TValueListEditor
        Left = 0
        Top = 97
        Width = 518
        Height = 411
        Align = alClient
        TabOrder = 1
        ExplicitWidth = 502
        ColWidths = (
          150
          362)
        RowHeights = (
          18
          18)
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Add Attribute'
      ImageIndex = 2
      ExplicitWidth = 469
      DesignSize = (
        518
        508)
      object AddAtrButton: TButton
        Left = 24
        Top = 16
        Width = 75
        Height = 25
        Caption = 'AddAtrButton'
        TabOrder = 0
        OnClick = AddAtrButtonClick
      end
      object AtrNameLEdit: TLabeledEdit
        Left = 24
        Top = 64
        Width = 121
        Height = 21
        EditLabel.Width = 65
        EditLabel.Height = 13
        EditLabel.Caption = 'AtrNameLEdit'
        TabOrder = 1
      end
      object AtrValLEdit: TLabeledEdit
        Left = 24
        Top = 112
        Width = 121
        Height = 21
        EditLabel.Width = 61
        EditLabel.Height = 13
        EditLabel.Caption = 'LabeledEdit1'
        TabOrder = 2
      end
      object Button3: TButton
        Left = 467
        Top = 3
        Width = 48
        Height = 22
        Anchors = [akTop, akRight]
        Cancel = True
        Caption = 'Button3'
        ModalResult = 2
        TabOrder = 3
        ExplicitLeft = 448
      end
    end
  end
end
