unit atrBrowserForm_u;
//12/22/17 This unit works in atrBrowser
//Attribute Manager is an extension of Datacad example utility originally published
//by Mark Madura of Datacad.
//Extensions written by Joseph Sosnowski 2018.
interface

uses
  Classes, Controls, Dialogs, Forms,
  Graphics, Grids,  Messages, StdCtrls, SysUtils, ValEdit,
  Variants, Windows, Vcl.ExtCtrls, Vcl.ComCtrls, inifiles,
  attr_u,
  AttrMngrUtil_u,
  urecords,
  uconstants,
  uinterfaces,
  stringUtil_u;
type
  TAttrMngrForm = class(TForm)
    AtrValueListEditor: TValueListEditor;
    Panel1: TPanel;
    DwgCheckBox: TCheckBox;
    SymCheckBox: TCheckBox;
    EntCheckBox: TCheckBox;
    LyrCheckBox: TCheckBox;
    VueCheckBox: TCheckBox;
    DtlCheckBox: TCheckBox;
    RefreshAtrButton: TButton;
    EntityAtrButton: TButton;
    Label1: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel2: TPanel;
    EntValueListEditor: TValueListEditor;
    EntityButton: TButton;
    TabSheet3: TTabSheet;
    AddAtrButton: TButton;
    AtrNameLEdit: TLabeledEdit;
    AtrValLEdit: TLabeledEdit;
    RadioGroup1: TRadioGroup;
    ActiveLyrCheckBox: TCheckBox;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RefreshAtrButtonClick(Sender: TObject);
    procedure EntityAtrButtonClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure EntityButtonClick(Sender: TObject);
    procedure AddAtrButtonClick(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
  private
     { Private declarations }
    af_lr : pMain_LR;  //Main Form LR
    af_dr : pAttrMngr_DR;  //Form DR Form - if needed
    procedure onFormMove(var Msg: TMessage); message WM_EXITSIZEMOVE;
  public
     { Public declarations }
    doDrawing, doEntity, doLayer, doSymbol, doView, doDetail : boolean;
    displayMode : integer; //dmAll, dmEnt  dmNone
//    theEnt : entity;
    procedure AddAtr;
    procedure ShowDrawingAtr;
    procedure ShowEntity;
    procedure ShowEntityAtr;
    procedure ShowLayerAtr;
    procedure ShowAllAtr;
    procedure ShutDown();
    procedure Startup();
    constructor Create(AOwner: TComponent;
        const P_LR: pMain_LR; const P_DR : pAttrMngr_DR);    reintroduce;
  end;

procedure AttrMngrFormStart  (P_LR: pMain_LR; P_DR : pAttrMngr_DR);
procedure AttrMngrFormOnHide (P_LR: pMain_LR; P_DR : pAttrMngr_DR);
procedure AttrMngrFormShow   (P_LR: pMain_LR; P_DR : pAttrMngr_DR);
procedure AttrMngrFormFinish (P_LR: pMain_LR; P_DR : pAttrMngr_DR);
procedure AttrMngrFormExit;

var
  AttrMngrForm : TAttrMngrForm;
  AttrMngrTop, AttrMngrLeft, AttrMngrHeight, AttrMngrWidth : integer;  //form size and location
implementation
{$R *.dfm}

procedure AttrMngrFormStart (P_LR: pMain_LR; P_DR : pAttrMngr_DR);
begin
  //required code:
  if not P_LR.formLoaded then
    AttrMngrForm := TAttrMngrForm.create(Application, P_LR, P_DR);  //

  //macro activities
  P_LR.formLoaded := true;
  AttrMngrForm.displayMode := dmNone;

//TODO THIS MAY DUPLICATE INITIALIZATION
  AttrMngrForm.Startup;
  //get previous popup location
  AttrMngrForm.Top := AttrMngrTop;
  AttrMngrForm.Left := AttrMngrLeft;
  AttrMngrForm.Width := AttrMngrWidth;
  AttrMngrForm.Height := AttrMngrHeight;
  //load any required settings
  P_LR.FirstEsc := false;
//  P_LR.SecondEsc := false;
  P_LR.state := fsFormActive;
end;

procedure AttrMngrFormOnHide (P_LR: pMain_LR; P_DR : pAttrMngr_dr);
begin
  //preserve any settings needed
  //required code:
  AttrMngrForm.Shutdown; //save location
end;

procedure AttrMngrFormShow (P_LR: pMain_LR; P_DR : pAttrMngr_dr); //Move to Popup Unit
begin
  //macro activities here
  //required code:
  AttrMngrForm.Startup; //get location
  AttrMngrForm.showModal;
end;

procedure AttrMngrFormFinish(P_LR: pMain_LR; P_DR: pAttrMngr_DR);
begin
  //Macro activities before exiting

  //required code:
  if P_LR.formLoaded then begin
    //save form location
    AttrMngrtop :=  AttrMngrForm.Top;
    AttrMngrleft :=  AttrMngrForm.Left;
    AttrMngrWidth := AttrMngrForm.Width;
    AttrMngrheight := AttrMngrForm.Height;
  end;
  P_LR.state := fsFormExit;
  AttrMngrForm.ShutDown;
end;

procedure AttrMngrFormExit;
begin
  freeAndNil(AttrMngrForm);
end;


//TatrForm--------------------------------------------------------------------
constructor tAttrMngrForm.Create(AOwner: TComponent; const P_LR: pMain_LR; const P_DR : pAttrMngr_DR);
begin
  inherited Create(AOwner);
  af_lr := P_LR;
  af_dr := P_DR;
  //add form initialization here or in formCreate event
end;

procedure tAttrMngrForm.AddAtr;
var
  AtrDef : tAttrDef;
  Ent : Entity;
begin
  ent_get (ent, af_dr.Adr);
  if AtrNameLEdit.text <> '' then
    AtrDef.name := AtrNameLEdit.text;

  AtrDef.source.stype := stEnt;
  AtrDef.source.ent := ent;
  AtrDef.kind := atr_str;
  AtrDef.str := str80(AtrValLEdit.text);
  AtrDef.visible := true;  // or set text style and location
  WrtAttr2 (AtrDef, true);
end;

procedure TAttrMngrForm.AddAtrButtonClick(Sender: TObject);
begin
  displaymode := dmAddAtr;
  ModalResult:= asGetPnts; //see FormCLoseQuery
end;


procedure TAttrMngrForm.RadioGroup1Click(Sender: TObject);
begin
  case  RadioGroup1.ItemIndex of
    0 :
      begin
        ActiveLyrCheckBox.visible := false;
        displayMode := dspDrawing;//  = 10;
      end;
    1 :
      begin
        ActiveLyrCheckBox.visible := true;
        displayMode := dspLayer;// = 11;
      end;
    2 :
      begin
        ActiveLyrCheckBox.visible := false;
        displayMode := dspSym;// = 12;
      end;
    3 :
      begin
        ActiveLyrCheckBox.visible := false;
        displayMode := dspEnt;// = 13;
      end;
    4 :
      begin
        ActiveLyrCheckBox.visible := false;
        displayMode := dspView;// = 14;
      end;
    5 :
      begin
        ActiveLyrCheckBox.visible := false;
        displayMode := dspDetail;// = 15;
      end;
  end;
end;

procedure tAttrMngrForm.RefreshAtrButtonClick(Sender: TObject);
begin
  displayMode := dmAllAtr;
  FormShow (sender);
end;

procedure tAttrMngrForm.EntityAtrButtonClick(Sender: TObject);
//Single entity selected
begin
  displaymode := dmEntAtr;
  ModalResult:= asGetPnts; //see FormCLoseQuery
end;

procedure TAttrMngrForm.EntityButtonClick(Sender: TObject);
begin
  case RadioGroup1.ItemIndex of
  0 : ShowDrawingAtr;
  1: //dspLayer :;
    begin
      ShowLayerAtr
    end;
  2://dspView :;
    begin
    end;
  3: //dspSym :
    begin
    end;
  4: //dspDetail
    begin
    end;
  5: //dspEnt
    begin
      displaymode := dmEntity;
      ModalResult:= asGetPnts; //see FormCLoseQuery
    end;
  end;
end;

procedure tAttrMngrForm.showEntity;
var
  theEnt : entity;
  sym : symbol;
  i,j : integer;
begin
  EntValueListEditor.strings.Clear;
  ent_get (theEnt, af_dr.Adr);

  if theEnt.enttype = entsym then begin
//    sym_get (sym,theEnt.symnameaddr);
    sym_find (theEnt.symname, sym);
    EntValueListEditor.InsertRow('SYMBOL DEFINITION: ', '', True);
    EntValueListEditor.InsertRow('     Addr: ', addr2str(sym.addr), True);
    EntValueListEditor.InsertRow('     AddrSpc: ', pointer2str(sym.addrspc), True);
    EntValueListEditor.InsertRow('     name: ', string(sym.name), True);
    EntValueListEditor.InsertRow('     SymNameAddr: ', addr2str(sym.symnameaddr), True);
    EntValueListEditor.InsertRow('     frstEnt: ', addr2str(sym.frstent), True);
    EntValueListEditor.InsertRow('     LastEnt: ', addr2str(sym.lastEnt), True);
    EntValueListEditor.InsertRow('     FrstAtr: ', addr2str(sym.frstAtr), True);
    EntValueListEditor.InsertRow('     LastAtr: ', addr2str(sym.LastAtr), True);
    EntValueListEditor.InsertRow('     min Point: ', pnt2str(sym.min), True);
    EntValueListEditor.InsertRow('     max Point: ', pnt2str(sym.max), True);
    EntValueListEditor.InsertRow('     instances (num): ', inttostr(sym.num), True);
    EntValueListEditor.InsertRow('     refFlag: ', booltostr(sym.refFlag), True);
    EntValueListEditor.InsertRow('     sym_fill1: ', booltostr(sym.sym_fill1), True);
    EntValueListEditor.InsertRow('     count: ', inttostr(sym.cnt), True);
    EntValueListEditor.InsertRow('     Serial No: ', inttostr(sym.serialno), True);
    EntValueListEditor.InsertRow('     KnockOut: ', inttostr(sym.KnockOut), True);
  end;

  EntValueListEditor.InsertRow('SYMBOL ENTITY: ', '', True);
  EntValueListEditor.InsertRow('     Type: ', EntTyp2Str(theEnt.enttype), True);
  EntValueListEditor.InsertRow('     Line Type: ', Line2Str(theEnt.enttype), True);
  EntValueListEditor.InsertRow('     Width: ', inttostr(theEnt.Width), True);
  EntValueListEditor.InsertRow('     Spacing: ', floattostr(theEnt.spacing), True);
  EntValueListEditor.InsertRow('     Overshoot: ', floattostr(theEnt.ovrshut), True);
  EntValueListEditor.InsertRow('     Attr: ', inttostr(theEnt.attr), True);
  EntValueListEditor.InsertRow('     Color: ', DcadClr2str(theEnt.color), True);
  EntValueListEditor.InsertRow('     VisLvl: ', inttostr(theEnt.visLvl), True);
  EntValueListEditor.InsertRow('     addr: ', addr2str(theEnt.addr), True);
  EntValueListEditor.InsertRow('     addrSpc: ', pointer2str(theEnt.addrSpc), True);
  EntValueListEditor.InsertRow('     Next: ', addr2str(theEnt.next), True);
  EntValueListEditor.InsertRow('     Prev: ', addr2str(theEnt.prev), True);
  EntValueListEditor.InsertRow('     Layer: ', addr2str(theEnt.lyr), True);
  EntValueListEditor.InsertRow('     User 0: ', inttostr(theEnt.user0), True);
  EntValueListEditor.InsertRow('     User 1: ', inttostr(theEnt.user1), True);
  EntValueListEditor.InsertRow('     User 2: ', inttostr(theEnt.user2), True);
  EntValueListEditor.InsertRow('     User 3: ', inttostr(theEnt.user3), True);
  EntValueListEditor.InsertRow('     Sel. Set: ', inttostr(theEnt.ss), True);
  EntValueListEditor.InsertRow('     First Attr: ', addr2str(theEnt.frstAtr), True);
  EntValueListEditor.InsertRow('     Last Attr: ', addr2str(theEnt.lastAtr), True);
  EntValueListEditor.InsertRow('     Index: ', inttostr(theEnt.index), True);
  EntValueListEditor.InsertRow('     Wall Index: ', inttostr(theEnt.wallindex), True);
  EntValueListEditor.InsertRow('     Last Wall Index: ', inttostr(theEnt.lastwallindex), True);
  EntValueListEditor.InsertRow('     Hatched: ', booltostr(theEnt.hatched), True);
  EntValueListEditor.InsertRow('     OC2 Material: ', inttostr(theEnt.o2cmaterialindex), True);
  case theent.enttype of
    entlin:
      begin
        EntValueListEditor.InsertRow('LINE Pnt1: ', pnt2str(theEnt.linpt1), True);
        EntValueListEditor.InsertRow('LINE Pnt2: ', pnt2str(theEnt.linpt2), True);
      end;
    entcrc:
      begin
        EntValueListEditor.InsertRow('CIRCLE Radius: ', floattostr(theEnt.crcRad), True);
        EntValueListEditor.InsertRow('CIRCLE Begin Angle: ', floattostr(theEnt.crcbang), True);
        EntValueListEditor.InsertRow('CIRCLE End Angle: ', floattostr(theEnt.crceang), True);
        EntValueListEditor.InsertRow('CIRCLE Base: ', floattostr(theEnt.crcbase), True);
        EntValueListEditor.InsertRow('CIRCLE Height: ', floattostr(theEnt.crchite), True);
      end;
    entbez:
      begin
        EntValueListEditor.InsertRow('BEZIER NumPnts: ', inttostr(theEnt.beznpnt), True);
        for i := 1 to theEnt.beznpnt do
          EntValueListEditor.InsertRow('BEZIER Pnt'+ inttostr (i)+ ': ', pnt2str(theEnt.bsppnt[i]), True);

        EntValueListEditor.InsertRow('BEZIER Base: ', floattostr(theEnt.bezbase), True);
        EntValueListEditor.InsertRow('BEZIER Height: ', floattostr(theEnt.bezhite), True);
      end;
    entarc:
      begin
        EntValueListEditor.InsertRow('ARC Radius: ', floattostr(theEnt.arcRad), True);
        EntValueListEditor.InsertRow('ARC Begin Angle: ', floattostr(theEnt.arcbang), True);
        EntValueListEditor.InsertRow('ARC End Angle: ', floattostr(theEnt.arceang), True);
        EntValueListEditor.InsertRow('ARC Base: ', floattostr(theEnt.arcbase), True);
        EntValueListEditor.InsertRow('ARC Height: ', floattostr(theEnt.archite), True);
      end;
    entmrk:
      begin
        EntValueListEditor.InsertRow('MARK Vector: ', pnt2str(theEnt.mrkvec), True);
        EntValueListEditor.InsertRow('MARK Type: ', inttostr(theEnt.mrktyp), True);
        EntValueListEditor.InsertRow('MARK Size: ', inttostr(theEnt.mrksiz), True);
      end;
    entell:
      begin
        EntValueListEditor.InsertRow('ELLIPSE Center: ', pnt2str(theEnt.ellcent), True);
        EntValueListEditor.InsertRow('ELLIPSE Radius: ', pnt2str(theEnt.ellRad), True);
        EntValueListEditor.InsertRow('ELLIPSE Begin Angle: ', floattostr(theEnt.ellbang), True);
        EntValueListEditor.InsertRow('ELLIPSE End Angle: ', floattostr(theEnt.elleang), True);
        EntValueListEditor.InsertRow('ELLIPSE Angle: ', floattostr(theEnt.ellang), True);
        EntValueListEditor.InsertRow('ELLIPSE Base: ', floattostr(theEnt.ellbase), True);
        EntValueListEditor.InsertRow('ELLIPSE Height: ', floattostr(theEnt.ellhite), True);
      end;
    enttxt:
      begin
        EntValueListEditor.InsertRow('TEXT Pnt1: ', pnt2str(theEnt.txtpnt), True);
        EntValueListEditor.InsertRow('TEXT Pnt1: ', pnt2str(theEnt.txtpnt2), True);
        EntValueListEditor.InsertRow('TEXT Type-backwards: ', booltostr(theEnt.txttype.backwards), True);
        EntValueListEditor.InsertRow('TEXT Type-upsideDn: ', booltostr(theEnt.txttype.upsidedown), True);
        EntValueListEditor.InsertRow('TEXT Type-justification: ', inttostr(theEnt.txttype.justification), True);
        EntValueListEditor.InsertRow('TEXT Type-Vert Align: ', inttostr(theEnt.txttype.VerticalAlignment), True);
        EntValueListEditor.InsertRow('TEXT Size: ', floattostr(theEnt.txtsiz), True);
        EntValueListEditor.InsertRow('TEXT Angle: ', floattostr(theEnt.txtang), True);
        EntValueListEditor.InsertRow('TEXT Slant: ', floattostr(theEnt.txtslant), True);
        EntValueListEditor.InsertRow('TEXT Aspect: ', floattostr(theEnt.txtaspect), True);
        EntValueListEditor.InsertRow('TEXT Base: ', floattostr(theEnt.txtBase), True);
        EntValueListEditor.InsertRow('TEXT Height: ', floattostr(theEnt.txtHite), True);
        EntValueListEditor.InsertRow('TEXT Font: ', string(theEnt.txtfon), True);
//        EntValueListEditor.InsertRow('TEXT Windows Font: ', booltostr(theEnt.TxtWinfont), True);
        EntValueListEditor.InsertRow('TEXT Font Number: ', inttostr(theEnt.fontNum), True);
        EntValueListEditor.InsertRow('TEXT String: ', string(theEnt.txtstr), True);
      end;
    entln3:
      begin
       EntValueListEditor.InsertRow('3D LINE Pnt1: ', pnt2str(theEnt.ln3pt1), True);
       EntValueListEditor.InsertRow('3D LINE Pnt2: ', pnt2str(theEnt.ln3pt2), True);
      end;
    entbsp:
      begin
        EntValueListEditor.InsertRow('BSPLINE NumPnts: ', inttostr(theEnt.bspnpnt), True);
        for i := 1 to theEnt.beznpnt do
          EntValueListEditor.InsertRow('BSPLINE Pnt'+ inttostr (i)+ ': ', pnt2str(theEnt.bsppnt[i]), True);

        EntValueListEditor.InsertRow('BSPLINE Base: ', floattostr(theEnt.bspbase), True);
        EntValueListEditor.InsertRow('BSPLINE Height: ', floattostr(theEnt.bsphite), True);
      end;
    entdim:
      begin
        EntValueListEditor.InsertRow('DIMENSIONS: ', 'Not Implemented', True);
      end;
    entsym:
      begin
        EntValueListEditor.InsertRow('SYMBOL Name: ', string(theEnt.symname), True);
        EntValueListEditor.InsertRow('SYMBOL Name Address: ', addr2str(theEnt.symnameaddr), True);
        EntValueListEditor.InsertRow('SYMBOL Attr Address: ', addr2str(theEnt.symattrib_addr), True);
        EntValueListEditor.InsertRow('SYMBOL Start: ', addr2str(theEnt.symstrt), True);
        EntValueListEditor.InsertRow('SYMBOL NewFactor ', inttostr(theEnt.NewFactor), True);
      end;
    entxref:
      begin
        EntValueListEditor.InsertRow('XREF Address: ', addr2str(theEnt.xrefnameaddr), True);
        EntValueListEditor.InsertRow('XREF refSpc: ', inttostr(theEnt.xrefSpc), True);
      end;
    enthol:
      begin
        EntValueListEditor.InsertRow('ENTHOL numPnts: ', inttostr(theEnt.holnpnt), True);
        EntValueListEditor.InsertRow('ENTHOL Thickness: ', pnt2str(theEnt.holthick), True);
        EntValueListEditor.InsertRow('ENTHOL Slab: ', booltostr(theEnt.holslab), True);
        EntValueListEditor.InsertRow('ENTHOL Convex: ', booltostr(theEnt.holconvex), True);
        for i := 1 to theEnt.holnpnt do begin
          EntValueListEditor.InsertRow('ENTHOL Pnt '+inttostr (i)+': ', pnt2str(theEnt.holPnt [i]), True);
          EntValueListEditor.InsertRow('ENTHOL PntIsLine '+inttostr (i)+': ', booltostr(theEnt.holisln[i]), True);
        end;
        EntValueListEditor.InsertRow('ENTHOL First Void: ', addr2str(theEnt.holfrstvoid), True);
        EntValueListEditor.InsertRow('ENTHOL Lastt Void: ', addr2str(theEnt.hollastvoid), True);
      end;
    entar3:
      begin
        EntValueListEditor.InsertRow('3D ARC Radius: ', floattostr (theEnt.ar3rad), True);
        EntValueListEditor.InsertRow('3D ARC Begin Angle: ', floattostr (theEnt.ar3bang), True);
        EntValueListEditor.InsertRow('3D ARC End Angle: ', floattostr (theEnt.ar3eang), True);
        EntValueListEditor.InsertRow('3D ARC Close: ', booltostr(theEnt.ar3close), True);
      end;
    entply:
      begin
        EntValueListEditor.InsertRow('POLYGON NumPnts: ', inttostr(theEnt.plynpnt), True);
        EntValueListEditor.InsertRow('POLYGON Thickness: ', pnt2str(theEnt.plythick), True);
        EntValueListEditor.InsertRow('POLYGON Slab: ', booltostr(theEnt.plyslab), True);
        EntValueListEditor.InsertRow('POLYGON Convex: ', booltostr(theEnt.plyconvex), True);
        for i := 1 to theEnt.plynpnt do begin
          EntValueListEditor.InsertRow('POLYGON Pnt '+inttostr (i)+': ', pnt2str(theEnt.plyPnt [i]), True);
          EntValueListEditor.InsertRow('POLYGON PntIsLine '+inttostr (i)+': ', booltostr(theEnt.plyisln [i]), True);
        end;
        EntValueListEditor.InsertRow('POLYGON First Void: ', addr2str(theEnt.plyfrstvoid), True);
        EntValueListEditor.InsertRow('POLYGON Lastt Void: ', addr2str(theEnt.plylastvoid), True);
      end;
    entcyl:
      begin
        EntValueListEditor.InsertRow('CYLINDER Radius: ', floattostr (theEnt.cylrad), True);
        EntValueListEditor.InsertRow('CYLINDER Length: ', floattostr (theEnt.cyllen), True);
        EntValueListEditor.InsertRow('CYLINDER Begin Angle: ', floattostr (theEnt.cylbang), True);
        EntValueListEditor.InsertRow('CYLINDER End Angle: ', floattostr (theEnt.cyleang), True);
        EntValueListEditor.InsertRow('CYLINDER Close: ', booltostr(theEnt.cylclose), True);
      end;
    entcon:
      begin
        EntValueListEditor.InsertRow('CONE Radius: ', floattostr (theEnt.conrad), True);
        EntValueListEditor.InsertRow('CONE Center: ', pnt2str(theEnt.concent), True);
        EntValueListEditor.InsertRow('CONE Begin Angle: ', floattostr (theEnt.conbang), True);
        EntValueListEditor.InsertRow('CONE End Angle: ', floattostr (theEnt.coneang), True);
        EntValueListEditor.InsertRow('CONE Close: ', booltostr(theEnt.conclose), True);
      end;
    entcnt:
      begin
        EntValueListEditor.InsertRow('CONTOUR: ', 'Not Implemented', True);
      end;
    enttor:
      begin
        EntValueListEditor.InsertRow('TORUS Division 1: ', inttostr(theEnt.tordiv1), True);
        EntValueListEditor.InsertRow('TORUS Division 2: ', inttostr(theEnt.tordiv2), True);
        EntValueListEditor.InsertRow('TORUS Radius 1: ', floattostr (theEnt.torrad1), True);
        EntValueListEditor.InsertRow('TORUS Radius 2: ', floattostr (theEnt.torrad2), True);
        EntValueListEditor.InsertRow('TORUS BAngle 1: ', floattostr (theEnt.torbang1), True);
        EntValueListEditor.InsertRow('TORUS BAngle 2: ', floattostr (theEnt.torbang2), True);
        EntValueListEditor.InsertRow('TORUS EAngle 1: ', floattostr (theEnt.torenag1), True);
        EntValueListEditor.InsertRow('TORUS EAngle 2: ', floattostr (theEnt.toreang2), True);
        EntValueListEditor.InsertRow('TORUS Close: ', booltostr(theEnt.torclose), True);
      end;
    entdom:
      begin
        EntValueListEditor.InsertRow('DOME Divisions 1: ', inttostr(theEnt.domdiv1), True);
        EntValueListEditor.InsertRow('DOME Divisions 2: ', inttostr(theEnt.domdiv2), True);
        EntValueListEditor.InsertRow('DOME Radius: ', floattostr (theEnt.domrad), True);
        EntValueListEditor.InsertRow('DOME BAngle 1: ', floattostr (theEnt.dombang1), True);
        EntValueListEditor.InsertRow('DOME BAngle 2: ', floattostr (theEnt.dombang2), True);
        EntValueListEditor.InsertRow('DOME Angle 1: ', floattostr(theEnt.domeang1), True);
        EntValueListEditor.InsertRow('DOME Angle 2: ', floattostr(theEnt.domeang2), True);
        EntValueListEditor.InsertRow('DOME Close: ', booltostr(theEnt.domclose), True);
      end;
    entsrf:
      begin

        for i := 1 to 4 do begin
          for j := 1 to 4 do begin
            EntValueListEditor.InsertRow('3D MESH Pnt['+inttostr (i)+ ','+inttostr (j)+ ']:', pnt2str(theEnt.srfpnt [i,j]), True);
          end;
        end;
        EntValueListEditor.InsertRow('3D MESH S Division: ', inttostr(theEnt.srfsdiv), True);
        EntValueListEditor.InsertRow('3D MESH T Division: ', inttostr(theEnt.srftdiv), True);
      end;
    entblk:
      begin
        for i := 1 to 4 do begin
          EntValueListEditor.InsertRow('BLOCK Pnt'+ inttostr (i)+ ': ', pnt2str(theEnt.blkpnt[i]), True);
        end;

      end;
    entslb:
      begin
        EntValueListEditor.InsertRow('SLAB NumPnts: ', inttostr(theEnt.slbnpnt), True);
        EntValueListEditor.InsertRow('SLAB Thickness: ', pnt2str(theEnt.slbthick), True);
        EntValueListEditor.InsertRow('SLAB Slab: ', booltostr(theEnt.slab), True);
        EntValueListEditor.InsertRow('SLAB Convex: ', booltostr(theEnt.convex), True);
        for i := 1 to theEnt.slbnpnt do begin
          EntValueListEditor.InsertRow('SLAB Pnt '+inttostr (i)+': ', pnt2str(theEnt.slbPnt [i]), True);
          EntValueListEditor.InsertRow('SLAB PntIsLine '+inttostr (i)+': ', booltostr(theEnt.slbisln[i]), True);
        end;
        EntValueListEditor.InsertRow('SLAB First Void: ', addr2str(theEnt.slbfrstvoid), True);
        EntValueListEditor.InsertRow('SLAB Lastt Void: ', addr2str(theEnt.slblastvoid), True);
      end;
    enttrn:
      begin
        EntValueListEditor.InsertRow('TRUNC CONE Center: ', pnt2str(theEnt.trncent), True);
        EntValueListEditor.InsertRow('TRUNC CONE Radius 1: ', floattostr (theEnt.trnrad1), True);
        EntValueListEditor.InsertRow('TRUNC CONE Radius 2: ', floattostr (theEnt.trnrad2), True);
        EntValueListEditor.InsertRow('TRUNC CONE Begin Angle: ', floattostr (theEnt.trnbang), True);
        EntValueListEditor.InsertRow('TRUNC CONE End Angle: ', floattostr (theEnt.trneang), True);
       end;
    entpln:
      begin
        EntValueListEditor.InsertRow('POLYLINE First Address: ', addr2str(theEnt.plnfrst), True);
        EntValueListEditor.InsertRow('POLYLINE Last Address: ', addr2str(theEnt.plnlast), True);
        EntValueListEditor.InsertRow('POLYLINE Close: ', booltostr(theEnt.plnclose), True);
        EntValueListEditor.InsertRow('POLYLINE Unused: ', booltostr(theEnt.plnunused), True);
        EntValueListEditor.InsertRow('BSPLINE Base: ', floattostr(theEnt.plbase), True);
        EntValueListEditor.InsertRow('BSPLINE Height: ', floattostr(theEnt.plnhite), True);
        EntValueListEditor.InsertRow('POLYLINE Voids: ', booltostr(theEnt.plnVoids), True);
        EntValueListEditor.InsertRow('POLYLINE Cover: ', booltostr(theEnt.plnCover), True);
        EntValueListEditor.InsertRow('POLYLINE First Void: ', addr2str(theEnt.plnFrstvoid), True);
        EntValueListEditor.InsertRow('POLYLINE Last Void: ', addr2str(theEnt.plinLastvoid), True);
        EntValueListEditor.InsertRow('POLYLINE In Type: ', inttostr(theEnt.plinType), True);
      end;
    entrev:
      begin
        EntValueListEditor.InsertRow('SURF of REV Begin Angle: ', floattostr(theEnt.revbang), True);
        EntValueListEditor.InsertRow('SURF of REV End Angle: ', floattostr(theEnt.reveang), True);
        EntValueListEditor.InsertRow('SURF of REV Divisions 1: ', inttostr(theEnt.revdiv1), True);
        EntValueListEditor.InsertRow('SURF of REV Divisions 2: ', inttostr(theEnt.revdiv2), True);
        EntValueListEditor.InsertRow('SURF of REV Rev First: ', addr2str(theEnt.revfrst), True);
        EntValueListEditor.InsertRow('SURF of REV Rev Last: ', addr2str(theEnt.revlast), True);
        EntValueListEditor.InsertRow('SURF of REV Type: ', inttostr(theEnt.revtype), True);
      end;
    entBlkAttDef:
      begin
        EntValueListEditor.InsertRow('BLKATTDEF Text Point: ', pnt2str(theEnt.Atttxtpnt), True);
        EntValueListEditor.InsertRow('BLKATTDEF Text Point 2: ', pnt2str(theEnt.Atttxtpnt2), True);
        EntValueListEditor.InsertRow('BLKATTDEF TxtType-backwards: ', booltostr(theEnt.atttxttype.backwards), True);
        EntValueListEditor.InsertRow('BLKATTDEF TxtType-upsideDn: ', booltostr(theEnt.atttxttype.upsidedown), True);
        EntValueListEditor.InsertRow('BLKATTDEF TxtType-justification: ', inttostr(theEnt.atttxttype.justification), True);
        EntValueListEditor.InsertRow('BLKATTDEF TxtType-Vert Align: ', inttostr(theEnt.atttxttype.VerticalAlignment), True);
        EntValueListEditor.InsertRow('BLKATTDEF TxtSize: ', floattostr(theEnt.Atttxtsiz), True);
        EntValueListEditor.InsertRow('BLKATTDEF TxtAngle: ', floattostr(theEnt.Attang), True);
        EntValueListEditor.InsertRow('BLKATTDEF TxtSlant: ', floattostr(theEnt.Attslant), True);
        EntValueListEditor.InsertRow('BLKATTDEF TxtAspect: ', floattostr(theEnt.Attaspect), True);
        EntValueListEditor.InsertRow('BLKATTDEF TxtBase: ', floattostr(theEnt.AttChrBase), True);
        EntValueListEditor.InsertRow('BLKATTDEF TxtHeight: ', floattostr(theEnt.AttchrHite), True);
        EntValueListEditor.InsertRow('BLKATTDEF Font: ', string(theEnt.Attfont), True);
//        EntValueListEditor.InsertRow('BLKATTDEF Windows Font: ', booltostr(theEnt.AttWinfont), True);
        EntValueListEditor.InsertRow('BLKATTDEF Font Number: ', inttostr(theEnt.AttfontNum), True);
        EntValueListEditor.InsertRow('BLKATTDEF Flags: ', inttostr(theEnt.AttFlags), True);
        EntValueListEditor.InsertRow('BLKATTDEF Unique Number: ', inttostr(theEnt.AttUniqueTagNum), True);
        EntValueListEditor.InsertRow('BLKATTDEF Tag: ', addr2str(theEnt.AttTag), True);
        EntValueListEditor.InsertRow('BLKATTDEF Prompt: ', addr2str(theEnt.Attprompt), True);
        EntValueListEditor.InsertRow('BLKATTDEF Default: ', addr2str(theEnt.AttDefault), True);
        EntValueListEditor.InsertRow('BLKATTDEF Symbol Data: ', addr2str(theEnt.AttSymbolData), True);
      end;
    DCNullObject, DCPlainTextObject, DCRTFTextObject, DCXLSObject, DCWallObject, DCDoorObject, DCWindowObject, DCCutOutObject, DCUnusedObject1,
            DCUnusedObject2, DCUnusedObject3, DCUnusedObject4, DCUnusedObject5, DCUnusedObject6, DCUnusedObject7, DCUnusedObject8:
      begin
          EntValueListEditor.InsertRow('SUPER ENT Object Found: ', '', True);
//        EntValueListEditor.InsertRow('SUPER ENT Object Type: ', addr2str(theEnt.ObjType), True);
//        EntValueListEditor.InsertRow('SUPER ENT Data 1: ', addr2str(theEnt.SuperData1), True);
//        EntValueListEditor.InsertRow('SUPER ENT Data 2: ', addr2str(theEnt.SuperData2), True);
      end;

  end;
//  EntValueListEditor.InsertRow('ATTRIBUTES ', '', True);

end;


procedure tAttrMngrForm.ShowLayerAtr;
var
  AtrName : string;
  atr :     attrib;
  AtrAddr,  LyrAddr : lgl_addr;
  layer :   Rlayer;
  atrString : string;
begin
  EntValueListEditor.strings.Clear;
  lyrAddr := getlyrcurr;
  lyr_get (LyrAddr, Layer);
  EntValueListEditor.InsertRow('CURRENT LAYER: ' , string(layer.name), True);
  EntValueListEditor.InsertRow('     Addr: ' , addr2str(Layer.addr), True);
  EntValueListEditor.InsertRow('     AddrSpc: ', pointer2str(Layer.addrspc), True);
  EntValueListEditor.InsertRow('     Nxt: ' , addr2str(Layer.nxt), True);
  EntValueListEditor.InsertRow('     Prev: ' , addr2str(Layer.prev), True);
  EntValueListEditor.InsertRow('     FrstAtr: ' , addr2str(Layer.frstAtr), True);
  EntValueListEditor.InsertRow('     LastAtr: ' , addr2str(Layer.lastAtr), True);
  EntValueListEditor.InsertRow('     Chg: ' , bool2str(Layer.chg), True);
  EntValueListEditor.InsertRow('     On: ' , bool2str(Layer.on), True);
  EntValueListEditor.InsertRow('     Num: ' , inttostr(Layer.num), True);
  EntValueListEditor.InsertRow('     Cnt: ' , inttostr(Layer.cnt), True);
  EntValueListEditor.InsertRow('     Color: ' , DcadClr2str(Layer.cnt), True);
  EntValueListEditor.InsertRow('     FirstLn: ' , addr2str(Layer.firstln), True);
  EntValueListEditor.InsertRow('     LastLn: ' , addr2str(Layer.lastln), True);
  EntValueListEditor.InsertRow('     GridOrg: ' , pnt2str(Layer.gridorg), True);
  EntValueListEditor.InsertRow('     ShowDivs: ' , pnt2str(Layer.showdivs), True);
  EntValueListEditor.InsertRow('     Shw1Divs: ' , pnt2str(Layer.shw1divs), True);
  EntValueListEditor.InsertRow('     SnapDivs: ' , pnt2str(Layer.SnapDivs), True);
  EntValueListEditor.InsertRow('     SnapGrid: ' , bool2str(Layer.snapgrid), True);
  EntValueListEditor.InsertRow('     ShowGrid: ' , bool2str(Layer.showgrid), True);
  EntValueListEditor.InsertRow('     Shw1Grid: ' , bool2str(Layer.shw1grid), True);
  EntValueListEditor.InsertRow('     Stupid03: ' , bool2str(Layer.stupid03), True);
  EntValueListEditor.InsertRow('     NumDivs: ' , inttostr(Layer.numdivs), True);
  EntValueListEditor.InsertRow('     GridAng: ' , floattostr(Layer.gridang), True);
  EntValueListEditor.InsertRow('     OnOFf: ' , inttostr(Layer.onoff), True);
  EntValueListEditor.InsertRow('     Group: ' , inttostr(Layer.group), True);
  EntValueListEditor.InsertRow('     Min: ' , pnt2str(Layer.min), True);
  EntValueListEditor.InsertRow('     Max: ' , pnt2str(Layer.max), True);
  EntValueListEditor.InsertRow('     RefPnt: ' , pnt2str(Layer.refpnt), True);
  EntValueListEditor.InsertRow('     LastIndex: ' , inttostr(Layer.lastindex), True);
  EntValueListEditor.InsertRow('     MaterialIndex: ' , inttostr(Layer.materialindex), True);
  EntValueListEditor.InsertRow('ATTRIBUTES: ' , '', True);
  AtrAddr := layer.frstatr;
  LyrAddr := layer.nxt; // lyr_next(LyrAddr);
  while atr_get(atr, AtrAddr) do begin
    AtrName := string(atr.Name);
    if atr.atrtype = atr_int then atrString := IntToStr(atr.int)
    else if atr.atrtype = atr_str then atrString := string(atr.str)
    else if atr.atrtype = atr_rl then atrString := FloatToStr(atr.dis)
    else if atr.atrtype = atr_dis then atrString := FloatToStr(atr.dis)
    else if atr.atrtype = atr_ang then atrString := FloatToStr(atr.ang)
    else if atr.atrtype = atr_pnt then atrString :=
          'x: ' + FloatToStr(atr.pnt.x) + ' y: ' + FloatToStr(atr.pnt.y) +
          ' z: ' + FloatToStr(atr.pnt.z)
    else if atr.atrtype = atr_str255 then atrString := string(atr.shstr);
       AtrAddr := atr.Next; // atr_next(atr);
    EntValueListEditor.InsertRow('Layer: '  + string(layer.name) + AtrName,
                                  atrString, True);
  end;
end;

procedure tAttrMngrForm.ShowDrawingAtr;
var
  AtrName : string;
  path, dname : str255;
  atr :     attrib;
  AtrAddr : lgl_addr;
  atrString : string;
  FillProp : TFillProp;
begin
  EntValueListEditor.strings.Clear;

  //Get Drawing Attributes
  AtrAddr := atr_sysfirst;
  getpath (path, pathcad);
  dwgname (dname);
  entValueListEditor.InsertRow('DRAWING: ', string(path + dname), True);

  while atr_get(atr, AtrAddr) do begin
    AtrName := string(atr.Name);
    if AtrName = 'DC_FILL' then begin
      move(atr.shstr, FillProp, length(atr.shstr));
      entValueListEditor.InsertRow('Drawing: Fill Color - ' +
         AtrName, IntToStr(FillProp.FillClr), True);
      entValueListEditor.InsertRow('Drawing: Pattern Color - ' +
         AtrName, IntToStr(FillProp.PatternClr), True);
      entValueListEditor.InsertRow('Drawing: Fill Pattern - ' +
         AtrName, IntToStr(FillProp.FillPattern), True);
      entValueListEditor.InsertRow('Drawing: Bitmap Fill - ' +
         AtrName, BoolToStr(FillProp.dobmp, True), True);
      entValueListEditor.InsertRow('Drawing: Fill Color = Entity - ' +
         AtrName, BoolToStr(FillProp.DoEntFillClr, True), True);
      entValueListEditor.InsertRow('Drawing: Pattern Color = Entity - ' +
         AtrName, BoolToStr(FillProp.DoEntPatternClr, True), True);
      entValueListEditor.InsertRow('Drawing: Fixed Image Aspect - ' +
         AtrName, BoolToStr(FillProp.ImgFixAspect, True), True);
      entValueListEditor.InsertRow('Drawing: Bitmap Fill Name - ' +
         AtrName, string(FillProp.ImageName), True);
    end
    else if atr.atrtype = atr_int then atrString := IntToStr(atr.int)
    else if atr.atrtype = atr_str then atrString := string(atr.str)
    else if atr.atrtype = atr_rl then atrString := FloatToStr(atr.dis)
    else if atr.atrtype = atr_dis then atrString := FloatToStr(atr.dis)
    else if atr.atrtype = atr_ang then atrString := FloatToStr(atr.ang)
    else if atr.atrtype = atr_pnt then atrString :=
          'x: ' + FloatToStr(atr.pnt.x) + ' y: ' + FloatToStr(atr.pnt.x) +
          ' z: ' + FloatToStr(atr.pnt.x)
    else if atr.atrtype = atr_str255 then atrString := string(atr.shstr);

    AtrAddr := atr.Next; //  atr_next(atr);


    entValueListEditor.InsertRow('     ' + AtrName, atrString, True);
  end;
end;

procedure tAttrMngrForm.ShowAllAtr;
var
  AtrName : string;
  atr :     attrib;
  AtrAddr, EntAddr, LyrAddr, SymAddr, VueAddr, DtlAddr : lgl_addr;
  atrString : string;
  ent :     entity;
  //lyr :     lgl_addr;
  layer :   Rlayer;
  sym :     sym_type;
  mode :    mode_type;
  SL :      TStringList;
  FillProp : TFillProp;
  Vue :     view_type;

begin
  AtrValueListEditor.strings.Clear;
  if DwgCheckBox.Checked then begin
    //Get Drawing Attributes
    AtrAddr := atr_sysfirst;
    while atr_get(atr, AtrAddr) do begin
      AtrName := string(atr.Name);
      if AtrName = 'DC_FILL' then begin
        move(atr.shstr, FillProp, length(atr.shstr));
        AtrValueListEditor.InsertRow('Drawing: Fill Color - ' +
           AtrName, IntToStr(FillProp.FillClr), True);
        AtrValueListEditor.InsertRow('Drawing: Pattern Color - ' +
           AtrName, IntToStr(FillProp.PatternClr), True);
        AtrValueListEditor.InsertRow('Drawing: Fill Pattern - ' +
           AtrName, IntToStr(FillProp.FillPattern), True);
        AtrValueListEditor.InsertRow('Drawing: Bitmap Fill - ' +
           AtrName, BoolToStr(FillProp.dobmp, True), True);
        AtrValueListEditor.InsertRow('Drawing: Fill Color = Entity - ' +
           AtrName, BoolToStr(FillProp.DoEntFillClr, True), True);
        AtrValueListEditor.InsertRow('Drawing: Pattern Color = Entity - ' +
           AtrName, BoolToStr(FillProp.DoEntPatternClr, True), True);
        AtrValueListEditor.InsertRow('Drawing: Fixed Image Aspect - ' +
           AtrName, BoolToStr(FillProp.ImgFixAspect, True), True);
        AtrValueListEditor.InsertRow('Drawing: Bitmap Fill Name - ' +
           AtrName, string(FillProp.ImageName), True);
      end
      else if atr.atrtype = atr_int then atrString := IntToStr(atr.int)
      else if atr.atrtype = atr_str then atrString := string(atr.str)
      else if atr.atrtype = atr_rl then atrString := FloatToStr(atr.dis)
      else if atr.atrtype = atr_dis then atrString := FloatToStr(atr.dis)
      else if atr.atrtype = atr_ang then atrString := FloatToStr(atr.ang)
      else if atr.atrtype = atr_pnt then atrString :=
            'x: ' + FloatToStr(atr.pnt.x) + ' y: ' + FloatToStr(atr.pnt.x) +
            ' z: ' + FloatToStr(atr.pnt.x)
      else if atr.atrtype = atr_str255 then atrString := string(atr.shstr);

      AtrAddr := atr.Next; //  atr_next(atr);
      AtrValueListEditor.InsertRow('Drawing: ' + AtrName, atrString, True);
    end;
  end;

  if EntCheckBox.Checked then begin
    // Get Entity Attributes
    mode_init(mode);
    mode_lyr(mode, lyr_all);  // Search all layers
    EntAddr := ent_first(mode);
    while ent_get(ent, EntAddr) do begin
      AtrAddr := ent.frstatr; // atr_entfirst(ent);
      EntAddr := ent_next(ent, mode);
      while atr_get(atr, AtrAddr) do begin
        AtrName := EntTyp2Str (ent.enttype) + '.'+ string(atr.Name);
        if AtrName = 'DC_FILL' then begin
          move(atr.shstr, FillProp, length(atr.shstr));
          AtrValueListEditor.InsertRow('Entity: Fill Color - ' +
             AtrName, IntToStr(FillProp.FillClr), True);
          AtrValueListEditor.InsertRow('Entity: Pattern Color - ' +
             AtrName, IntToStr(FillProp.PatternClr), True);
          AtrValueListEditor.InsertRow('Entity: Fill Pattern - ' +
             AtrName, IntToStr(FillProp.FillPattern), True);
          AtrValueListEditor.InsertRow('Entity: Bitmap Fill - ' +
             AtrName, BoolToStr(FillProp.dobmp, True), True);
          AtrValueListEditor.InsertRow('Entity: Fill Color = Entity - ' +
             AtrName, BoolToStr(FillProp.DoEntFillClr, True), True);
          AtrValueListEditor.InsertRow('Entity: Pattern Color = Entity - ' +
             AtrName, BoolToStr(FillProp.DoEntPatternClr, True), True);
          AtrValueListEditor.InsertRow('Entity: Fixed Image Aspect - ' +
             AtrName, BoolToStr(FillProp.ImgFixAspect, True), True);
          AtrValueListEditor.InsertRow('Entity: Bitmap Fill Name - ' +
             AtrName, string(FillProp.ImageName), True);
        end
        else begin
          if atr.atrtype = atr_int then atrString := IntToStr(atr.int)
          else if atr.atrtype = atr_str then atrString := string(atr.str)
          else if atr.atrtype = atr_rl then atrString := FloatToStr(atr.dis)
          else if atr.atrtype = atr_dis then atrString := FloatToStr(atr.dis)
          else if atr.atrtype = atr_ang then atrString := FloatToStr(atr.ang)
          else if atr.atrtype = atr_pnt then atrString :=
                'x: ' + FloatToStr(atr.pnt.x) + ' y: ' +
                FloatToStr(atr.pnt.x) + ' z: ' + FloatToStr(atr.pnt.x)
          else if atr.atrtype = atr_str255 then atrString := string(atr.shstr);
          AtrValueListEditor.InsertRow('Entity: ' + AtrName, atrString, True);
        end;
        AtrAddr := atr.Next; //  atr_next(atr);
      end;
    end;
  end;

  if LyrCheckBox.Checked then begin
    // Get Layer Attributes
    LyrAddr := lyr_first;
    while lyr_get(LyrAddr, layer) do begin
      AtrAddr := layer.frstatr;
      LyrAddr := layer.nxt; // lyr_next(LyrAddr);
      while atr_get(atr, AtrAddr) do begin
        AtrName := string(atr.Name);
        if atr.atrtype = atr_int then atrString := IntToStr(atr.int)
        else if atr.atrtype = atr_str then atrString := string(atr.str)
        else if atr.atrtype = atr_rl then atrString := FloatToStr(atr.dis)
        else if atr.atrtype = atr_dis then atrString := FloatToStr(atr.dis)
        else if atr.atrtype = atr_ang then atrString := FloatToStr(atr.ang)
        else if atr.atrtype = atr_pnt then atrString :=
              'x: ' + FloatToStr(atr.pnt.x) + ' y: ' + FloatToStr(atr.pnt.x) +
              ' z: ' + FloatToStr(atr.pnt.x)
        else if atr.atrtype = atr_str255 then atrString := string(atr.shstr);

        AtrAddr := atr.Next; // atr_next(atr);
        AtrValueListEditor.InsertRow('Layer: ' { + layer.name } +
           AtrName, atrString, True);
      end;
    end;
  end;

  if SymCheckBox.Checked then begin
    // Get Symbol Attributes
    SymAddr := sym_first;
    while sym_get(sym, SymAddr) do begin
      AtrAddr := sym.frstatr;
      SymAddr := sym.Next;
      while atr_get(atr, AtrAddr) do begin
        AtrName := string(atr.Name);
        if atr.atrtype = atr_int then atrString := IntToStr(atr.int)
        else if atr.atrtype = atr_str then atrString := string(atr.str)
        else if atr.atrtype = atr_rl then atrString := FloatToStr(atr.dis)
        else if atr.atrtype = atr_dis then atrString := FloatToStr(atr.dis)
        else if atr.atrtype = atr_ang then atrString := FloatToStr(atr.ang)
        else if atr.atrtype = atr_pnt then atrString :=
              'x: ' + FloatToStr(atr.pnt.x) + ' y: ' + FloatToStr(atr.pnt.x) +
              ' z: ' + FloatToStr(atr.pnt.x)
        else if atr.atrtype = atr_str255 then atrString := string(atr.shstr);

        AtrAddr := atr.Next; // atr_next(atr);
        AtrValueListEditor.InsertRow('Symbol: ' + AtrName, atrString, True);
      end;
    end;
  end;

  if VueCheckBox.Checked then begin
    VueAddr := PGSv.strtview;
    // Get GoTo View Attributes
    while view_get(Vue, VueAddr) do begin
      AtrAddr := Vue.frstatr;
      VueAddr := Vue.Next;
      while atr_get(atr, AtrAddr) do begin
        AtrAddr := atr.Next; // atr_next(atr);
        if Vue.flag1 = 0 then begin
          AtrName := string(atr.Name);
          if atr.atrtype = atr_int then atrString := IntToStr(atr.int)
          else if atr.atrtype = atr_str then atrString := string(atr.str)
          else if atr.atrtype = atr_rl then atrString := FloatToStr(atr.dis)
          else if atr.atrtype = atr_dis then atrString := FloatToStr(atr.dis)
          else if atr.atrtype = atr_ang then atrString := FloatToStr(atr.ang)
          else if atr.atrtype = atr_pnt then atrString :=
                'x: ' + FloatToStr(atr.pnt.x) + ' y: ' +
                FloatToStr(atr.pnt.x) + ' z: ' + FloatToStr(atr.pnt.x)
          else if atr.atrtype = atr_str255 then atrString := string(atr.shstr);

          AtrValueListEditor.InsertRow('GoTo View: ' + AtrName, atrString, True);
        end;
      end;
    end;
  end;

  if DtlCheckBox.Checked then begin
    DtlAddr := PGSv.strtview;
    // Get MSP Detail Attributes
    while view_get(Vue, DtlAddr) do begin
      AtrAddr := Vue.frstatr;
      DtlAddr := Vue.Next;
      while atr_get(atr, AtrAddr) do begin
        AtrAddr := atr.Next; // atr_next(atr);
        if Vue.flag1 = 1 then begin // flag2 contains sheet no.
          AtrName := string(atr.Name);
          if atr.atrtype = atr_int then atrString := IntToStr(atr.int)
          else if atr.atrtype = atr_str then atrString := string(atr.str)
          else if atr.atrtype = atr_rl then atrString := string(FloatToStr(atr.dis))
          else if atr.atrtype = atr_dis then atrString :=string(FloatToStr(atr.dis))
          else if atr.atrtype = atr_ang then atrString := FloatToStr(atr.ang)
          else if atr.atrtype = atr_pnt then atrString :=
                'x: ' + FloatToStr(atr.pnt.x) + ' y: ' +
                FloatToStr(atr.pnt.x) + ' z: ' + FloatToStr(atr.pnt.x)
          else if atr.atrtype = atr_str255 then atrString := string(atr.shstr);

          AtrValueListEditor.InsertRow('MSP Detail: ' + AtrName, atrString, True);
        end;
      end;
    end;
  end;

  // Sort Value List
  SL := TStringList.Create;
  SL.Assign(AtrValueListEditor.Strings);
  SL.Sort;
  AtrValueListEditor.Strings := SL;
end; //ShowAll

procedure tAttrMngrForm.ShowEntityAtr;
var
  ent : entity;
  atr :     attrib;
  AtrAddr : lgl_addr;
  atrString, AtrName : string;
  FillProp : TFillProp;
begin
  AtrValueListEditor.strings.Clear;
  ent_get (ent, af_dr.Adr);

  AtrAddr := ent.frstatr; // atr_entfirst(ent);
  while atr_get(atr, AtrAddr) do begin

    Label1.caption := addr2str (AtrAddr);
    AtrName := EntTyp2Str (ent.enttype) + '.'+ string(atr.Name) ;
    if AtrName = 'DC_FILL' then begin
      move(atr.shstr, FillProp, length(atr.shstr));
      AtrValueListEditor.InsertRow('Ent. Fill Attr.: Fill Color - ' +
        AtrName, IntToStr(FillProp.FillClr), True);
      AtrValueListEditor.InsertRow('Ent. Fill Attr.: Pattern Color - ' +
        AtrName, IntToStr(FillProp.PatternClr), True);
      AtrValueListEditor.InsertRow('Ent. Fill Attr.: Fill Pattern - ' +
        AtrName, IntToStr(FillProp.FillPattern), True);
      AtrValueListEditor.InsertRow('Ent. Fill Attr.: Bitmap Fill - ' +
        AtrName, BoolToStr(FillProp.dobmp, True), True);
      AtrValueListEditor.InsertRow('Ent. Fill Attr.: Fill Color = Entity - ' +
        AtrName, BoolToStr(FillProp.DoEntFillClr, True), True);
      AtrValueListEditor.InsertRow('Ent. Fill Attr.: Pattern Color = Entity - ' +
        AtrName, BoolToStr(FillProp.DoEntPatternClr, True), True);
      AtrValueListEditor.InsertRow('Ent. Fill Attr.: Fixed Image Aspect - ' +
        AtrName, BoolToStr(FillProp.ImgFixAspect, True), True);
      AtrValueListEditor.InsertRow('Ent. Fill Attr.: Bitmap Fill Name - ' +
        AtrName, string(FillProp.ImageName), True);
    end
    else  begin
      case atr.atrtype of
      atr_int: atrString := 'atr_int = ' + IntToStr(atr.int);
      atr_rl: atrString := 'atr_rl  = ' + FloatToStr(atr.dis);
      atr_str: atrString := 'atr_str  = ' + String(atr.str);
      atr_dis: atrString := 'atr_dis  = ' + FloatToStr(atr.dis);
      atr_ang: atrString := 'atr_ang  = ' + FloatToStr(atr.ang);
      atr_pnt: atrString := 'atr_pnt  = x: ' + FloatToStr(atr.pnt.x) + ', y: '
          + FloatToStr(atr.pnt.x) +', z: ' + FloatToStr(atr.pnt.x);
      atr_str255: atrString := 'atr_str255 = ' + string(atr.shstr);
      end;
    end;
    AtrValueListEditor.InsertRow('Ent. Attr.: ' + AtrName, atrString, True);
    AtrAddr := atr.Next; //  atr_next(atr);

  end;
end; //ShowEntity

procedure tAttrMngrForm.OnFormMove(var msg: TMessage);
begin
  AttrMngrTop := Top;
  AttrMngrLeft := Left;
  AttrMngrWidth := Width;
  AttrMngrHeight := Height;
end;

procedure TAttrMngrForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin

  case ModalResult of
   mrCancel:  //called from form 'X' to exit form
     begin
       if af_lr.menuMode then begin
         af_lr.state := msMenuSkipRouter; //return to menu operation
         ModalResult:= mrOK;  //menu is in command so only hide the form
       end
       else begin
//         showmessage ('Close query: Form commands cancel');
         af_lr.state := fsFormFinish; //exit menu

       end;
     end;
   asGetPnts:  //called by button to hide form only
     begin
       af_lr.gotPnts := false;
       af_lr.pntCount := 1;
       af_lr.Pnts2Get := 1;
       af_lr.state := msMenuSkipRouter; //return to menu operation
       ModalResult:= mrOK;
     end;
  end;
end;

procedure TAttrMngrForm.FormCreate(Sender: TObject);
begin
  Top := AttrMngrTop;
  Left := AttrMngrLeft;
  Width := AttrMngrWidth;
  Height := AttrMngrHeight;
end;

procedure TAttrMngrForm.FormResize(Sender: TObject);
begin
  AttrMngrTop := Top;
  AttrMngrLeft := Left;
  AttrMngrWidth := Width;
  AttrMngrHeight := Height;
end;

procedure TAttrMngrForm.FormShow(Sender: TObject);
begin
  case displaymode of
  dmNone:
    begin
      AtrValueListEditor.strings.Clear;
      EntValueListEditor.strings.Clear;
    end;
  dmEntAtr: showEntityAtr;
  dmAllAtr: showAllAtr;
  dmEntity : showEntity;
  dspEnt  : showEntity;
  dmAddAtr : AddAtr;
  end;

end;


procedure tAttrMngrForm.ShutDown();
//finalization procedure
var
  iniFl: tIniFile;
begin
  iniFl :=  TIniFile.Create(file_ini);  //create the class object
  try
    iniFl.writeinteger ('Atr FORM', 'Top', AttrMngrTop);
    iniFl.writeinteger ('Atr FORM', 'Left', AttrMngrLeft);
    iniFl.writeinteger ('Atr FORM', 'Width', AttrMngrWidth);
    iniFl.writeinteger ('Atr FORM', 'Height', AttrMngrHeight);
  finally
    iniFl.Free;
  end;
end;

procedure  tAttrMngrForm.Startup();
//initialization procedure
var
  iniFl: tIniFile;
begin
  iniFl :=  TIniFile.Create(file_ini);  //create the class object
  try
    AttrMngrTop := iniFl.ReadInteger('Atr FORM', 'Top', 100); //adjust default sizes as required
    AttrMngrLeft := iniFl.ReadInteger('Atr FORM', 'Left', 100);
    AttrMngrWidth := iniFl.ReadInteger('Atr FORM', 'Width', 440);
    AttrMngrHeight := iniFl.ReadInteger('Atr FORM', 'Height', 380);
  finally
    iniFl.Free;
  end;
end;


initialization
  AttrMngrForm.Startup;
finalization
  AttrMngrForm.ShutDown;
end.
